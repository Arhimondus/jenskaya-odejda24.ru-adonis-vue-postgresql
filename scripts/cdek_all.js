const axios = require('axios'),
convert = require('xml-js'),
pgp = require('pg-promise')(),
db = pgp(require('./db.config.js'));

(async () => {
	await db.none('TRUNCATE locations;');
	
	let regions = await db.query('SELECT * FROM regions');
	
	await load_all();

	process.exit();

	async function load_all() {
		let { data } = await axios.get(`https://integration.cdek.ru/pvzlist.php`);
		let deserialized = convert.xml2js(data, { compact: true, spaces: 0 });

		// let list_x = (Array.isArray(deserialized.PvzList.Pvz) ? deserialized.PvzList.Pvz : [deserialized.PvzList.Pvz]).map(m => m._attributes.RegionName).sort().filter(function(item, pos, ary) {
        // 	return !pos || item != ary[pos - 1];
		// });
		let list = (Array.isArray(deserialized.PvzList.Pvz) ? deserialized.PvzList.Pvz : [deserialized.PvzList.Pvz])
		.filter(pvz => pvz._attributes.IsDressingRoom === 'есть')
		.map(pvz => {
			let region = pvz._attributes.RegionName ? regions.find(r => r.cdek_name == pvz._attributes.RegionName) : null;
			if(!region) return null;

			let location = {};
			location.latitude = pvz._attributes.coordY;
			location.longitude = pvz._attributes.coordX;
			location.title = pvz._attributes.Name;
			location.description = `<p>${pvz._attributes.WorkTime}</p><p>${pvz._attributes.Note}</p>`;
			location.address = pvz._attributes.City + ', ' + pvz._attributes.Address;
			location.prefix = 'СДЭК';
			location.type = 'cdek';
			location.region = region.region_id;
			return location;
		})
		.filter((e) => e);

		const cs = new pgp.helpers.ColumnSet([
			'latitude',
			'longitude',
			'title',
			'description',
			'address',
			'type',
			'region',
			'prefix',
		], { table: 'locations' });
		
		await db.none(pgp.helpers.insert(list, cs));
		
		console.log(`load_all - ok`);
	}
})();

