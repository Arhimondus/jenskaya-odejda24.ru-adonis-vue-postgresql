const axios = require('axios'),
convert = require('xml-js'),
pgp = require('pg-promise')(),
db = pgp(require('./db.config.js'));

(async () => {
	await db.none('TRUNCATE locations;');
	
	let city_ids = [44, 1100, 1692, 469, 467, 14322, 114, 483, 520, 521, 1545, 468, 45, 46941, 47, 184, 472, 474, 920, 205, 493, 76, 307, 17, 1097, 389, 391, 57, 524, 1250, 1103, 516, 14494, 1299, 1095, 1174, 157, 162, 510];
	for(let city_id of city_ids) {
		try {
			load_city(city_id);
		} catch(err) {
			console.log(`!cannot get city_id ${city_id}`);
		}
	}
	
	async function load_city(city_id) {
		let { data } = await axios.get(`https://integration.cdek.ru/pvzlist.php?cityid=${city_id}`);
		let deserialized = convert.xml2js(data, { compact: true, spaces: 0 });
		let list = (Array.isArray(deserialized.PvzList.Pvz) ? deserialized.PvzList.Pvz : [deserialized.PvzList.Pvz])
		.filter(pvz => pvz._attributes.IsDressingRoom === 'есть')
		.map(pvz => {
			let location = {};
			location.latitude = pvz._attributes.coordY;
			location.longitude = pvz._attributes.coordX;
			location.title = pvz._attributes.Name;
			location.description = `<p>${pvz._attributes.WorkTime}</p><p>${pvz._attributes.Note}</p>`;
			location.address = pvz._attributes.City + ', ' + pvz._attributes.Address;
			location.prefix = 'СДЭК';
			location.type = 'cdek';
			return location;
		});
		const cs = new pgp.helpers.ColumnSet([
			'latitude',
			'longitude',
			'title',
			'description',
			'address',
			'type',
			'prefix',
		], { table: 'locations' });
		
		await db.none(pgp.helpers.insert(list, cs));
		
		console.log(`city_id #${city_id} - ok`);
	}
})();

