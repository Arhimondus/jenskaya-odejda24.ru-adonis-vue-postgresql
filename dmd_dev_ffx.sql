--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: orders_count_recalc(); Type: FUNCTION; Schema: public; Owner: dmd_fs_user
--

CREATE FUNCTION orders_count_recalc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
orders_count SMALLINT;
BEGIN
	--Routine body goes here...
	if(NEW.user_id IS NOT null) THEN
		SELECT COUNT(*) INTO orders_count FROM orders WHERE user_id = NEW.user_id AND status_id = 4;
		UPDATE users SET orders = orders_count WHERE user_id = NEW.user_id;
	END IF;
	RETURN NULL;
END
$$;


ALTER FUNCTION public.orders_count_recalc() OWNER TO dmd_fs_user;

--
-- Name: sizes_count_recalc(); Type: FUNCTION; Schema: public; Owner: dmd_fs_user
--

CREATE FUNCTION sizes_count_recalc() RETURNS trigger
    LANGUAGE plpgsql
    AS $$DECLARE
sizes_count SMALLINT;
BEGIN
	SELECT SUM((value::text::numeric)) INTO sizes_count FROM jsonb_each(NEW.sizes);
	UPDATE products SET count = sizes_count WHERE product_id = NEW.product_id;
	RETURN NULL;
END
$$;


ALTER FUNCTION public.sizes_count_recalc() OWNER TO dmd_fs_user;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: banners; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE banners (
    banner_id integer NOT NULL,
    uri character varying(255) NOT NULL,
    alt character varying(255),
    name character varying(255) DEFAULT '[Без названия]'::character varying,
    image character varying(255),
    "position" character varying(255),
    "order" smallint DEFAULT 0 NOT NULL
);


ALTER TABLE banners OWNER TO dmd_fs_user;

--
-- Name: banner_autoid; Type: SEQUENCE; Schema: public; Owner: dmd_fs_user
--

CREATE SEQUENCE banner_autoid
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE banner_autoid OWNER TO dmd_fs_user;

--
-- Name: banner_autoid; Type: SEQUENCE OWNED BY; Schema: public; Owner: dmd_fs_user
--

ALTER SEQUENCE banner_autoid OWNED BY banners.banner_id;


--
-- Name: colors; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE colors (
    color_id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE colors OWNER TO dmd_fs_user;

--
-- Name: incexps; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE incexps (
    incexp_id integer NOT NULL,
    changes jsonb,
    product_id integer,
    comment character varying(255),
    name character varying(255),
    date date DEFAULT now(),
    type character varying(255)
);


ALTER TABLE incexps OWNER TO dmd_fs_user;

--
-- Name: incexp_autoid; Type: SEQUENCE; Schema: public; Owner: dmd_fs_user
--

CREATE SEQUENCE incexp_autoid
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE incexp_autoid OWNER TO dmd_fs_user;

--
-- Name: incexp_autoid; Type: SEQUENCE OWNED BY; Schema: public; Owner: dmd_fs_user
--

ALTER SEQUENCE incexp_autoid OWNED BY incexps.incexp_id;


--
-- Name: lists; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE lists (
    list_id integer NOT NULL,
    name character varying(255),
    title character varying(255),
    uri character varying(255),
    "order" integer DEFAULT 0,
    color character varying(255),
    in_menu boolean NOT NULL,
    max_size smallint,
    type_id smallint,
    catalog_title character varying(255),
    "column" smallint,
    header_bg character varying,
    in_catalog boolean NOT NULL,
    in_main boolean NOT NULL,
    meta_description character varying(255),
    meta_keywords character varying(255),
    parent_id integer,
    articles json
);


ALTER TABLE lists OWNER TO dmd_fs_user;

--
-- Name: list_autoid; Type: SEQUENCE; Schema: public; Owner: dmd_fs_user
--

CREATE SEQUENCE list_autoid
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE list_autoid OWNER TO dmd_fs_user;

--
-- Name: list_autoid; Type: SEQUENCE OWNED BY; Schema: public; Owner: dmd_fs_user
--

ALTER SEQUENCE list_autoid OWNED BY lists.list_id;


--
-- Name: orders; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE orders (
    order_id integer NOT NULL,
    user_id integer,
    items jsonb NOT NULL,
    address jsonb NOT NULL,
    personal jsonb NOT NULL,
    comment character varying(255),
    datetime timestamp(0) without time zone DEFAULT now() NOT NULL,
    summary integer NOT NULL,
    delivery integer NOT NULL,
    total integer NOT NULL,
    status_id smallint DEFAULT 1 NOT NULL,
    promo character varying(255),
    promo_id integer,
    bonuses smallint,
    accrued smallint
);


ALTER TABLE orders OWNER TO dmd_fs_user;

--
-- Name: order_autoid; Type: SEQUENCE; Schema: public; Owner: dmd_fs_user
--

CREATE SEQUENCE order_autoid
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_autoid OWNER TO dmd_fs_user;

--
-- Name: order_autoid; Type: SEQUENCE OWNED BY; Schema: public; Owner: dmd_fs_user
--

ALTER SEQUENCE order_autoid OWNED BY orders.order_id;


--
-- Name: positions; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE positions (
    "position" character varying(255) NOT NULL
);


ALTER TABLE positions OWNER TO dmd_fs_user;

--
-- Name: products; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE products (
    product_id integer NOT NULL,
    name character varying(255) NOT NULL,
    seo_uri character varying(255),
    description text DEFAULT ''::character varying NOT NULL,
    price smallint NOT NULL,
    base_price smallint,
    sizes jsonb,
    color_id smallint,
    images jsonb DEFAULT '["no-image"]'::jsonb,
    tag character varying(255),
    lists jsonb,
    fields jsonb DEFAULT '[]'::jsonb,
    reviews jsonb DEFAULT '[]'::jsonb,
    questions jsonb DEFAULT '[]'::jsonb,
    special character varying(255),
    count_free smallint,
    article_number character varying(255) NOT NULL,
    type_id integer NOT NULL,
    discount smallint,
    count smallint DEFAULT 0
);


ALTER TABLE products OWNER TO dmd_fs_user;

--
-- Name: product_autoid; Type: SEQUENCE; Schema: public; Owner: dmd_fs_user
--

CREATE SEQUENCE product_autoid
    START WITH 40
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_autoid OWNER TO dmd_fs_user;

--
-- Name: product_autoid; Type: SEQUENCE OWNED BY; Schema: public; Owner: dmd_fs_user
--

ALTER SEQUENCE product_autoid OWNED BY products.product_id;


--
-- Name: promos; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE promos (
    promo_id integer NOT NULL,
    name character varying(255) NOT NULL,
    discount smallint DEFAULT 5 NOT NULL,
    last_date date,
    enabled boolean DEFAULT true NOT NULL,
    single_use boolean NOT NULL
);


ALTER TABLE promos OWNER TO dmd_fs_user;

--
-- Name: promo_autoid; Type: SEQUENCE; Schema: public; Owner: dmd_fs_user
--

CREATE SEQUENCE promo_autoid
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE promo_autoid OWNER TO dmd_fs_user;

--
-- Name: promo_autoid; Type: SEQUENCE OWNED BY; Schema: public; Owner: dmd_fs_user
--

ALTER SEQUENCE promo_autoid OWNED BY promos.promo_id;


--
-- Name: sizes; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE sizes (
    sizes jsonb
);


ALTER TABLE sizes OWNER TO dmd_fs_user;

--
-- Name: statuses; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE statuses (
    status_id integer NOT NULL,
    name character varying(255) NOT NULL,
    color character varying(255)
);


ALTER TABLE statuses OWNER TO dmd_fs_user;

--
-- Name: types; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE types (
    type_id integer NOT NULL,
    sizes jsonb,
    name character varying(255),
    size_table text
);


ALTER TABLE types OWNER TO dmd_fs_user;

--
-- Name: users; Type: TABLE; Schema: public; Owner: dmd_fs_user
--

CREATE TABLE users (
    user_id integer NOT NULL,
    name character varying(255) DEFAULT ''::character varying,
    password character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    activation_code character varying(255),
    address jsonb DEFAULT '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}'::jsonb,
    phone character varying(255) DEFAULT ''::character varying,
    bonuses smallint DEFAULT 0 NOT NULL,
    is_admin boolean DEFAULT false NOT NULL,
    orders smallint DEFAULT 0 NOT NULL,
    sms_subscribe boolean DEFAULT false,
    email_subscribe boolean DEFAULT false
);


ALTER TABLE users OWNER TO dmd_fs_user;

--
-- Name: user_autoid; Type: SEQUENCE; Schema: public; Owner: dmd_fs_user
--

CREATE SEQUENCE user_autoid
    START WITH 70
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_autoid OWNER TO dmd_fs_user;

--
-- Name: user_autoid; Type: SEQUENCE OWNED BY; Schema: public; Owner: dmd_fs_user
--

ALTER SEQUENCE user_autoid OWNED BY users.user_id;


--
-- Name: banners banner_id; Type: DEFAULT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY banners ALTER COLUMN banner_id SET DEFAULT nextval('banner_autoid'::regclass);


--
-- Name: incexps incexp_id; Type: DEFAULT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY incexps ALTER COLUMN incexp_id SET DEFAULT nextval('incexp_autoid'::regclass);


--
-- Name: lists list_id; Type: DEFAULT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY lists ALTER COLUMN list_id SET DEFAULT nextval('list_autoid'::regclass);


--
-- Name: orders order_id; Type: DEFAULT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY orders ALTER COLUMN order_id SET DEFAULT nextval('order_autoid'::regclass);


--
-- Name: products product_id; Type: DEFAULT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY products ALTER COLUMN product_id SET DEFAULT nextval('product_autoid'::regclass);


--
-- Name: promos promo_id; Type: DEFAULT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY promos ALTER COLUMN promo_id SET DEFAULT nextval('promo_autoid'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY users ALTER COLUMN user_id SET DEFAULT nextval('user_autoid'::regclass);


--
-- Name: banner_autoid; Type: SEQUENCE SET; Schema: public; Owner: dmd_fs_user
--

SELECT pg_catalog.setval('banner_autoid', 9, true);


--
-- Data for Name: banners; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY banners (banner_id, uri, alt, name, image, "position", "order") FROM stdin;
5	https://ya.ru	234234	Верхний 750x390	BkWZCGDKZ	main_top	1
4	rubashki	234234	Верхний 370x180	BkKTHGwK-	main_top	2
3	/dzhinsi	32523	Верхний 370x180	H1GWBfwYZ	main_top	3
1	banner_zlo_uri	альтернативное написание	260xN	Sk2Jg7PKW	list	0
2	rubashki-v-kletku	324234	260xN	BJuylmDKb	list	0
9	114	124124	Нижний 370x538	S1RSafvKb	main_bottom	4
8	2342423	2342423	Нижний 720x307	rJRN6fDtZ	main_bottom	3
7	23423	423423	Нижний 420x180	HJJ46MPFW	main_bottom	2
6	Баннер снизу	Баннер снизу	Нижний 420x180	ryagpGDF-	main_bottom	1
\.


--
-- Data for Name: colors; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY colors (color_id, name) FROM stdin;
1	Белый
2	Чёрный
3	Красный
4	Синий
\.


--
-- Name: incexp_autoid; Type: SEQUENCE SET; Schema: public; Owner: dmd_fs_user
--

SELECT pg_catalog.setval('incexp_autoid', 16, true);


--
-- Data for Name: incexps; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY incexps (incexp_id, changes, product_id, comment, name, date, type) FROM stdin;
2	{"42": 5, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	expense
3	{"42": 5, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	income
4	{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 1, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	income
5	{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 1}	60	\N	Аделия Рубашка	2017-08-27	income
6	{"42": 1, "44": 1, "46": 1, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	income
7	{"42": 2, "44": 4, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	expense
8	{"42": 0, "44": 0, "46": 5, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	expense
9	{"42": 0, "44": 0, "46": 0, "48": 3, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	expense
10	{"42": 0, "44": 0, "46": 0, "48": 1, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	expense
11	{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 1, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	expense
12	{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 8}	60	\N	Аделия Рубашка	2017-08-27	income
13	{"42": 5, "44": 2, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	income
14	{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 15}	60	\N	Аделия Рубашка	2017-08-27	income
15	{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 3, "68": 0}	60	\N	Аделия Рубашка	2017-08-27	income
16	{"42": 0, "44": 0, "46": 0, "48": 5, "50": 5, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	62	\N	Джинсы длины 7/8 с принтом	2017-08-28	income
\.


--
-- Name: list_autoid; Type: SEQUENCE SET; Schema: public; Owner: dmd_fs_user
--

SELECT pg_catalog.setval('list_autoid', 29, true);


--
-- Data for Name: lists; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY lists (list_id, name, title, uri, "order", color, in_menu, max_size, type_id, catalog_title, "column", header_bg, in_catalog, in_main, meta_description, meta_keywords, parent_id, articles) FROM stdin;
17	Блузки	Купить блузки	bluzki	0		f	\N	2	Каталог блузок	1	rJG7xATOW	t	f	описание	блузки хорошие, блузки отличные	\N	[{"header":"Суперзаголовок мега плюс XXL","content":"235235","tab":"Вкладка"},{"header":"Заголовок2","content":"235235","tab":"Вкладка"},{"header":"Суперзаголовок мега плюс XXL","content":"23523523","tab":"Вкладка"},{"header":"Заголовок4","content":"235235","tab":"Вкладка"},{"header":"Заголовок5","content":"dfgdfg","tab":"Вкладка"}]
24	Рубашки	123	rubashki	1	\N	f	4	2	Каталог рубашек	1	ryOpZJRO-	t	f	123	123	\N	[{"header":"how to reliably","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"how to reliably"},{"header":"how to reliably","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"how to reliably"},{"header":"how to reliably","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"how to reliably"},{"header":"how to reliably","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"how to reliably"},{"header":"234","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"23"}]
26	Рубашки белые	2554545	rubashki-belie	1	\N	f	\N	\N	Офисные белые рубашки	\N	B1udLJAu-	f	f	3вапвапвап	2353255	24	[{"header":"234324","content":"ё","tab":"Статья"},{"header":"","content":"","tab":"Статья2"},{"header":"","content":"","tab":"Статья2"},{"header":"","content":"","tab":"Статья2"},{"header":"124","content":"214","tab":"Статья9"}]
25	Рубашки в клетку	впрв	rubashki-v-kletku	2	\N	f	\N	\N	Рубашки в клетку с доставкой	\N	S1DJXy0uW	f	f	орпаро	апропаро	24	[{"header":"fccvv","content":"fccvv","tab":"sdfsd"},{"header":"fccvvfccvv","content":"fccvv","tab":"fccvv"},{"header":"fccvv","content":"fccvv","tab":"fccvv"},{"header":"fccvvfccvvfccvv","content":"fccvvfccvvfccvvfccvv","tab":"fccvvfccvv"},{"header":"fccvv","content":"fccvvfccvv","tab":"fccvv"}]
27	Кофты	кофты	kofti	3	\N	f	\N	2	Каталог кофт	2	B1xovkRdb	t	f	кофты описание	кофты хорошие	\N	[{"header":"стат1","content":"стат1","tab":"стат1"},{"header":"стат1","content":"стат1","tab":"стат1"},{"header":"стат1","content":"стат1","tab":"стат1"},{"header":"стат1","content":"стат1","tab":"стат1"},{"header":"стат1","content":"стат1","tab":"стат1"}]
28	Главная	Главная		\N	\N	f	\N	\N	Главная	\N	\N	f	t	Главная	Главная	\N	[{"header":"2Женщина3234","content":"В интернет-магазине Lamoda.ru представлены лучшие коллекции женской одежды, обуви и аксессуаров, а также косметики и парфюмов ведущих мировых брендов.\\nУвлечённые модницы могут выбрать товары премиум-класса и органично совместить их с базовыми вещами демократичной ценовой категории. Любая п","tab":"Женщинам"},{"header":"3Мужчинам45","content":"В интернет-магазине Lamoda.ru представлены лучшие коллекции женской одежды, обуви и аксессуаров, а также косметики и парфюмов ведущих мировых брендов.\\nУвлечённые модницы могут выбрать товары премиум-класса и органично совместить их с базовыми вещами демократичной ценовой категории. Любая п","tab":"Мужчинам"},{"header":"34534","content":"В интернет-магазине Lamoda.ru представлены лучшие коллекции женской одежды, обуви и аксессуаров, а также косметики и парфюмов ведущих мировых брендов.\\nУвлечённые модницы могут выбрать товары премиум-класса и органично совместить их с базовыми вещами демократичной ценовой категории. Любая п","tab":"Сумкам"},{"header":"Блузхкам","content":"В интернет-магазине Lamoda.ru представлены лучшие коллекции женской одежды, обуви и аксессуаров, а также косметики и парфюмов ведущих мировых брендов.\\nУвлечённые модницы могут выбрать товары премиум-класса и органично совместить их с базовыми вещами демократичной ценовой категории. Любая п","tab":"Блузхкам"},{"header":"3Юбкам45","content":"Интернет-магазин женской одежды, обуви, аксессуаров, косметики и парфюмерии","tab":"Юбкам"}]
29	Новинки	Новинки	novinki	1	\N	t	\N	\N	Новинки	\N	S1lEqGwtZ	f	f	Новинки	Новинки	\N	[{"header":"","content":"","tab":""},{"header":"","content":"","tab":""},{"header":"","content":"","tab":""},{"header":"","content":"","tab":""},{"header":"","content":"","tab":""}]
\.


--
-- Name: order_autoid; Type: SEQUENCE SET; Schema: public; Owner: dmd_fs_user
--

SELECT pg_catalog.setval('order_autoid', 40, true);


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY orders (order_id, user_id, items, address, personal, comment, datetime, summary, delivery, total, status_id, promo, promo_id, bonuses, accrued) FROM stdin;
25	\N	[{"id": 1, "name": "Джинсы модные", "size": "44", "product_id": 63, "article_number": "432", "computed_price": 5173}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "rr", "email": "artemiy@a.rr", "phone": "r"}	\N	2017-08-31 02:06:20	5173	0	5173	3	\N	\N	0	\N
27	\N	[{"id": 1, "name": "Джинсы модные", "size": "44", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 2, "name": "Аделия Рубашка", "size": "66", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:21:46	6173	0	6173	3	\N	\N	0	\N
28	\N	[{"id": 1, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"id": 2, "name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 1222}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "Архонт", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:27:04	2222	250	2472	3	\N	\N	0	\N
29	\N	[{"id": 1, "name": "Джинсы модные", "size": "66", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 2, "name": "Джинсы модные", "size": "66", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 3, "name": "Джинсы модные", "size": "66", "product_id": 63, "article_number": "432", "computed_price": 5173}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:32:45	15519	0	15519	3	\N	\N	0	\N
31	\N	[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:38:23	5173	0	5173	3	\N	\N	0	\N
32	\N	[{"id": 1, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:40:06	1000	250	1250	3	\N	\N	0	\N
33	\N	[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}]	{"city": "343", "code": "234", "flat": "124124124", "house": "34134", "porch": "234", "street": "234", "building": "324"}	{"name": "sdfsfsd", "email": "abudabi@asd.ru", "phone": "234234"}	\N	2017-08-31 02:42:39	5173	0	5173	3	\N	\N	0	\N
34	\N	[{"id": 1, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"id": 2, "name": "Аделия Рубашка", "size": "66", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"id": 3, "name": "Аделия Рубашка", "size": "66", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:44:46	3000	0	3000	3	\N	\N	0	\N
36	77	[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}]	{"city": "", "code": "", "flat": "24", "house": "", "porch": "", "street": "", "building": ""}	{"name": "", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:53:14	5173	0	5173	3	\N	\N	0	\N
35	77	[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 2, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]	{"city": "", "code": "", "flat": "214124", "house": "124", "porch": "124", "street": "", "building": ""}	{"name": "", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:49:04	6173	0	6173	3	\N	\N	0	\N
20	1	[{"name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1143}, {"name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 745}, {"name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 745}, {"name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 745}]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-08-29 15:59:46	3378	0	1689	4	\N	\N	\N	\N
18	1	[{"name": "Аделия Рубашка", "size": "68", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1143}, {"name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 745}]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-08-29 15:40:20	1888	250	1194	3	\N	\N	\N	\N
17	1	[{"name": "Аделия Рубашка", "size": "64", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1143}, {"name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 745}]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-08-29 15:34:26	1888	250	1194	4	\N	\N	\N	\N
21	1	[{"name": "Аделия Рубашка", "size": "44", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1593}, {"name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 1038}]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-08-29 16:31:48	2631	0	1316	4	PROMOCODE2017	7	1315	\N
24	\N	[{"id": 1, "name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 1222}, {"id": 2, "name": "Аделия Рубашка", "size": "64", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"id": 3, "name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 1222}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "214", "email": "artemiytm@yandex.ru", "phone": "24"}	\N	2017-08-31 01:33:20	3444	0	3444	2	\N	\N	0	\N
22	1	[{"name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"name": "Аделия Рубашка", "size": "64", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"name": "Аделия Рубашка", "size": "44", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-08-29 18:55:02	3000	250	1750	3	\N	\N	1500	\N
19	1	[{"name": "Аделия Рубашка", "size": "68", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1875}, {"name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1875}, {"name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1875}]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-08-29 15:40:52	5625	0	3125	4	\N	\N	\N	\N
23	1	[{"id": 1, "name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 1222}]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-08-31 00:57:00	1222	250	1472	3	\N	\N	0	\N
26	\N	[{"id": 1, "name": "Джинсы модные", "size": "64", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 2, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "Abudabi", "email": "arar@aaasr.abudabi", "phone": "123123213"}	\N	2017-08-31 02:16:40	6173	0	6173	3	\N	\N	0	\N
30	\N	[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}]	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	{"name": "", "email": "abudabi@asd.ru", "phone": ""}	\N	2017-08-31 02:34:57	5173	0	5173	3	\N	\N	0	\N
38	1	[]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита333", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-09-01 14:14:14	0	250	250	3	ZIMA2018	8	0	\N
37	1	[]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-09-01 13:59:46	0	250	250	3	\N	\N	0	\N
39	1	[]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Марга33рита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	\N	2017-09-01 14:41:01	3155	0	3155	3	ZIMA2018	8	0	\N
40	1	[]	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	{"name": "Марга3353", "email": "artemiytm@ya.ru", "phone": "+79057641220"}	1241212412	2017-09-01 18:33:38	0	250	250	3	\N	\N	0	\N
\.


--
-- Data for Name: positions; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY positions ("position") FROM stdin;
list
main_top
main_bottom
\.


--
-- Name: product_autoid; Type: SEQUENCE SET; Schema: public; Owner: dmd_fs_user
--

SELECT pg_catalog.setval('product_autoid', 63, true);


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY products (product_id, name, seo_uri, description, price, base_price, sizes, color_id, images, tag, lists, fields, reviews, questions, special, count_free, article_number, type_id, discount, count) FROM stdin;
60	Аделия Рубашка	\N	выаапп вапав пвап авп вап вап вап  вап вап вапвыаапп вапав пвап авп вап вап вап  вап вап вапвыаапп вапав п <a href="/">adadasd ссылка </a>вап авп вап вап вап  вап вап вапвыаапп вапав пвап авп вап вап вап  вап вап вапвыаапп вапав пвап авп вап вап вап  вап вап вап3434	1325	2500	{"42": 5, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 3, "68": 24}	3	["ryDVwgRdZ", "HJu4vx0d-", "SklO4DeRd-", "BkK4DlRuZ", "Sy5NwlAOZ"]	NEW	[24, 25, 27, 28]	[{"name": "Длина", "value": "456"}, {"name": "Длина", "value": "456"}, {"name": "Длина", "value": "3"}, {"name": "Длина", "value": "34"}, {"name": "Длина", "value": "34"}, {"name": "Длина", "value": "34"}, {"name": "Длина", "value": "53"}, {"name": "Длина", "value": "345345345345345345"}]	[{"date": "2017-08-29", "name": "124124", "rate": 5, "text": "124", "user_id": 1}, {"date": "2017-08-29", "name": "Экимора", "rate": 3, "text": "1123123", "user_id": 1}, {"date": "2017-08-29", "name": "Левиафан", "rate": 1, "text": "Просто отвратительный товар!", "user_id": 1}]	[{"date": "2017-08-29", "name": "Лейкемия", "text": "Мой вопрос. Почему это отвартительнейший товар ещё здесь????\\n", "user_id": 1}]	\N	3	ФАфыа23	2	47	32
62	Джинсы длины 7/8 с принтом	\N	ender API — это специальный интерфейс для разработчиков, позволяющий интегрировать возможности электронной рассылки практически с любым открытым веб-сервисом или desktop-приложением. Наш API предоставляет возможности для управления списками подписчиков, создания и отправки разных типов сообщений, получения доступа к статистике, а также для работы партнёров.	11837	13300	{"42": 0, "44": 0, "46": 0, "48": 3, "50": 4, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}	2	["HJ6QucZFW", "rJJVucbYW", "ryxNdcWtZ"]	ДА!	[28]	[{"name": "Длина", "value": "Великая"}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}]	[{"date": "2017-08-29", "name": "234324", "rate": 1, "text": "234234", "user_id": 1}]	[]	\N	3	уае642	3	11	7
63	Джинсы модные	\N	Джинсы модные	4799	5333	{"42": 0, "44": 1, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 1, "66": 3, "68": 24}	1	["rJMjoxXK-"]	4	[17, 24, 27]	[{"name": "35", "value": "35"}, {"name": "355", "value": "353"}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}]	[]	[]	\N	2	432	2	10	29
\.


--
-- Name: promo_autoid; Type: SEQUENCE SET; Schema: public; Owner: dmd_fs_user
--

SELECT pg_catalog.setval('promo_autoid', 8, true);


--
-- Data for Name: promos; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY promos (promo_id, name, discount, last_date, enabled, single_use) FROM stdin;
7	PROMOCODE2017	15	\N	t	t
8	ZIMA2018	39	2017-09-16	t	f
\.


--
-- Data for Name: sizes; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY sizes (sizes) FROM stdin;
{"42": 0, "44": 0, "46": 0, "48": 5, "50": 5, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0, "[object Object]": 0}
\.


--
-- Data for Name: statuses; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY statuses (status_id, name, color) FROM stdin;
1	Новый	#4ac7ac
3	Отменён	rgba(179, 179, 179, 0.75)
2	В процессе	rgb(255, 123, 159)
5	Возращённый	#7eb3b0
4	Завершён	rgb(84, 181, 60)
\.


--
-- Data for Name: types; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY types (type_id, sizes, name, size_table) FROM stdin;
2	[{"name": "42 / 44 RUS", "size": "42"}, {"name": "44 / 46 RUS", "size": "44"}, {"name": "46 / 48 RUS", "size": "46"}, {"name": "48 / 50 RUS", "size": "48"}, {"name": "50 / 52 RUS", "size": "50"}, {"name": "52 / 54 RUS", "size": "52"}, {"name": "54 / 56 RUS", "size": "54"}, {"name": "56 / 58 RUS", "size": "56"}, {"name": "58 / 60 RUS", "size": "58"}, {"name": "60 / 62 RUS", "size": "60"}, {"name": "62 / 64 RUS", "size": "62"}, {"name": "64 / 66 RUS", "size": "64"}, {"name": "66 / 68 RUS", "size": "66"}, {"name": "68 / 70 RUS", "size": "68"}]	Одежда-верх	<table class="size-table">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">Обхват груди (см)</td>\r\n\t\t\t<td>82-86</td>\r\n\t\t\t<td>86-90</td>\r\n\t\t\t<td>90-94</td>\r\n\t\t\t<td>94-98</td>\r\n\t\t\t<td>98-102</td>\r\n\t\t\t<td>102-106</td>\r\n\t\t\t<td>106-110</td>\r\n\t\t\t<td>110-114</td>\r\n\t\t\t<td>114-118</td> <!--58-->\r\n\t\t\t<td>118-122</td>\r\n\t\t\t<td>122-126</td>\r\n\t\t\t<td>126-130</td>\r\n\t\t\t<td>130-134</td>\r\n\t\t\t<td>134-138</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">Обхват талии (см)</td>\r\n\t\t\t<td>62-66</td>\r\n\t\t\t<td>66-70</td>\r\n\t\t\t<td>70-74</td>\r\n\t\t\t<td>74-78</td>\r\n\t\t\t<td>78-82</td>\r\n\t\t\t<td>82-86</td>\r\n\t\t\t<td>86-90</td>\r\n\t\t\t<td>90-95</td>\r\n\t\t\t<td>95-100</td> <!--58-->\r\n\t\t\t<td>100-105</td>\r\n\t\t\t<td>105-110</td>\r\n\t\t\t<td>110-115</td>\r\n\t\t\t<td>115-120</td>\r\n\t\t\t<td>120-125</td>\r\n\t\t</tr>\r\n\t\t<tr class="size-table__bold">\r\n\t\t\t<td class="size-table__left-align">Российский размер (RUS)</td>\r\n\t\t\t<td>42</td>\r\n\t\t\t<td>44</td>\r\n\t\t\t<td>46</td>\r\n\t\t\t<td>48</td>\r\n\t\t\t<td>50</td>\r\n\t\t\t<td>52</td>\r\n\t\t\t<td>54</td>\r\n\t\t\t<td>56</td>\r\n\t\t\t<td>58</td> <!--58-->\r\n\t\t\t<td>60</td>\r\n\t\t\t<td>62</td>\r\n\t\t\t<td>64</td>\r\n\t\t\t<td>66</td>\r\n\t\t\t<td>68</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align  size-table__bold">Китайский</td>\r\n\t\t\t<td>M</td>\r\n\t\t\t<td>L</td>\r\n\t\t\t<td>L</td>\r\n\t\t\t<td>XL</td>\r\n\t\t\t<td>2XL</td>\r\n\t\t\t<td>2XL</td>\r\n\t\t\t<td>3XL</td>\r\n\t\t\t<td>3XL</td>\r\n\t\t\t<td>4XL</td>  <!--58-->\r\n\t\t\t<td>4XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">Европа &nbsp;(EUR)</td>\r\n\t\t\t<td>34</td>\r\n\t\t\t<td>36</td>\r\n\t\t\t<td>38</td>\r\n\t\t\t<td>40</td>\r\n\t\t\t<td>42</td>\r\n\t\t\t<td>44</td>\r\n\t\t\t<td>46</td>\r\n\t\t\t<td>48</td>\r\n\t\t\t<td>50</td>  <!--58-->\r\n\t\t\t<td>52</td>\r\n\t\t\t<td>54</td>\r\n\t\t\t<td>56</td>\r\n\t\t\t<td>58</td>\r\n\t\t\t<td>60</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">США (USA)</td>\r\n\t\t\t<td>4</td>\r\n\t\t\t<td>6</td>\r\n\t\t\t<td>8</td>\r\n\t\t\t<td>10</td>\r\n\t\t\t<td>12</td>\r\n\t\t\t<td>14</td>\r\n\t\t\t<td>16</td>\r\n\t\t\t<td>18</td>  \r\n\t\t\t<td>20</td> <!--58-->\r\n\t\t\t<td>22</td>\r\n\t\t\t<td>24</td>\r\n\t\t\t<td>26</td>\r\n\t\t\t<td>28</td>\r\n\t\t\t<td>30</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">Международный (INT)</td>\r\n\t\t\t<td>XS</td>\r\n\t\t\t<td>S</td>\r\n\t\t\t<td>M</td>\r\n\t\t\t<td>M</td>\r\n\t\t\t<td>L</td>\r\n\t\t\t<td>XL</td>\r\n\t\t\t<td>XL</td>\r\n\t\t\t<td>2XL</td>\r\n\t\t\t<td>3XL</td> <!--58-->\r\n\t\t\t<td>4XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t\t<td>6XL</td>\r\n\t\t\t<td>7XL</td>\r\n\t\t\t<td>8XL</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>
3	[{"name": "42 / 44 RUS", "size": "42"}, {"name": "44 / 46 RUS", "size": "44"}, {"name": "46 / 48 RUS", "size": "46"}, {"name": "48 / 50 RUS", "size": "48"}, {"name": "50 / 52 RUS", "size": "50"}, {"name": "52 / 54 RUS", "size": "52"}, {"name": "54 / 56 RUS", "size": "54"}, {"name": "56 / 58 RUS", "size": "56"}, {"name": "58 / 60 RUS", "size": "58"}, {"name": "60 / 62 RUS", "size": "60"}, {"name": "62 / 64 RUS", "size": "62"}, {"name": "64 / 66 RUS", "size": "64"}, {"name": "66 / 68 RUS", "size": "66"}, {"name": "68 / 70 RUS", "size": "68"}]	Одежда-низ	<table class="size-table">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">Обхват бёдер (см)</td>\r\n\t\t\t<td>88-92</td>\r\n\t\t\t<td>92-96</td>\r\n\t\t\t<td>96-100</td>\r\n\t\t\t<td>100-104</td>\r\n\t\t\t<td>104-108</td>\r\n\t\t\t<td>108-112</td>\r\n\t\t\t<td>112-116</td>\r\n\t\t\t<td>116-120</td>\r\n\t\t\t<td>120-124</td> <!--58-->\r\n\t\t\t<td>124-128</td>\r\n\t\t\t<td>128-132</td>\r\n\t\t\t<td>132-136</td>\r\n\t\t\t<td>136-140</td>\r\n\t\t\t<td>140-144</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">Обхват талии (см)</td>\r\n\t\t\t<td>62-66</td>\r\n\t\t\t<td>66-70</td>\r\n\t\t\t<td>70-74</td>\r\n\t\t\t<td>74-78</td>\r\n\t\t\t<td>78-82</td>\r\n\t\t\t<td>82-86</td>\r\n\t\t\t<td>86-90</td>\r\n\t\t\t<td>90-95</td>\r\n\t\t\t<td>95-100</td> <!--58-->\r\n\t\t\t<td>100-105</td>\r\n\t\t\t<td>105-110</td>\r\n\t\t\t<td>110-115</td>\r\n\t\t\t<td>115-120</td>\r\n\t\t\t<td>120-125</td>\r\n\t\t</tr>\r\n\t\t<tr class="size-table__bold">\r\n\t\t\t<td class="size-table__left-align">Российский размер (RUS)</td>\r\n\t\t\t<td>42</td>\r\n\t\t\t<td>44</td>\r\n\t\t\t<td>46</td>\r\n\t\t\t<td>48</td>\r\n\t\t\t<td>50</td>\r\n\t\t\t<td>52</td>\r\n\t\t\t<td>54</td>\r\n\t\t\t<td>56</td>\r\n\t\t\t<td>58</td> <!--58-->\r\n\t\t\t<td>60</td>\r\n\t\t\t<td>62</td>\r\n\t\t\t<td>64</td>\r\n\t\t\t<td>66</td>\r\n\t\t\t<td>68</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align  size-table__bold">Китайский</td>\r\n\t\t\t<td>M</td>\r\n\t\t\t<td>L</td>\r\n\t\t\t<td>L</td>\r\n\t\t\t<td>XL</td>\r\n\t\t\t<td>2XL</td>\r\n\t\t\t<td>2XL</td>\r\n\t\t\t<td>3XL</td>\r\n\t\t\t<td>3XL</td>\r\n\t\t\t<td>4XL</td>  <!--58-->\r\n\t\t\t<td>4XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">Европа (EUR)</td>\r\n\t\t\t<td>34</td>\r\n\t\t\t<td>36</td>\r\n\t\t\t<td>38</td>\r\n\t\t\t<td>40</td>\r\n\t\t\t<td>42</td>\r\n\t\t\t<td>44</td>\r\n\t\t\t<td>46</td>\r\n\t\t\t<td>48</td>\r\n\t\t\t<td>50</td>  <!--58-->\r\n\t\t\t<td>52</td>\r\n\t\t\t<td>54</td>\r\n\t\t\t<td>56</td>\r\n\t\t\t<td>58</td>\r\n\t\t\t<td>60</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">США (USA)</td>\r\n\t\t\t<td>4</td>\r\n\t\t\t<td>6</td>\r\n\t\t\t<td>8</td>\r\n\t\t\t<td>10</td>\r\n\t\t\t<td>12</td>\r\n\t\t\t<td>14</td>\r\n\t\t\t<td>16</td>\r\n\t\t\t<td>18</td>  \r\n\t\t\t<td>20</td> <!--58-->\r\n\t\t\t<td>22</td>\r\n\t\t\t<td>24</td>\r\n\t\t\t<td>26</td>\r\n\t\t\t<td>28</td>\r\n\t\t\t<td>30</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td class="size-table__left-align size-table__bold">Международный (INT)</td>\r\n\t\t\t<td>XS</td>\r\n\t\t\t<td>S</td>\r\n\t\t\t<td>M</td>\r\n\t\t\t<td>M</td>\r\n\t\t\t<td>L</td>\r\n\t\t\t<td>XL</td>\r\n\t\t\t<td>XL</td>\r\n\t\t\t<td>2XL</td>\r\n\t\t\t<td>3XL</td> <!--58-->\r\n\t\t\t<td>4XL</td>\r\n\t\t\t<td>5XL</td>\r\n\t\t\t<td>6XL</td>\r\n\t\t\t<td>7XL</td>\r\n\t\t\t<td>8XL</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>
4	[{"name": "34 RUS", "size": "34"}, {"name": "34.5 RUS", "size": "34.5"}, {"name": "35 RUS", "size": "35"}, {"name": "36 RUS", "size": "36"}, {"name": "37 RUS", "size": "37"}, {"name": "37.5 RUS", "size": "37.5"}, {"name": "38 RUS", "size": "38"}, {"name": "38.5 RUS", "size": "38.5"}, {"name": "39 RUS", "size": "39"}, {"name": "40 RUS", "size": "40"}, {"name": "40.5 RUS", "size": "40.5"}, {"name": "41 RUS", "size": "41"}, {"name": "42 RUS", "size": "42"}, {"name": "43 RUS", "size": "43"}]	Обувь	\N
1	[{"name": "Безразмерный", "size": "∞"}]	Безразмерный	\N
\.


--
-- Name: user_autoid; Type: SEQUENCE SET; Schema: public; Owner: dmd_fs_user
--

SELECT pg_catalog.setval('user_autoid', 78, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: dmd_fs_user
--

COPY users (user_id, name, password, email, activation_code, address, phone, bonuses, is_admin, orders, sms_subscribe, email_subscribe) FROM stdin;
1	Маргарита	$2a$10$xXH1/v4J6YMjv1iQZWYGveBVbccUX7CEM2J4ab7XLbTnxUm5OD2eS	artemiytm@ya.ru	\N	{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}	+79057641220	0	f	4	f	t
74		$2a$10$vA/eAVgyL0J6P/baniW01.qW0fs7YHWiqwq1ozsv5ukSFGV42koqy	robot@gmail.com	rJwAyCR_b	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}		-2500	f	0	f	f
75	rr	Hy7xB6EKW	artemiy@a.rr	\N	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	r	0	f	0	f	f
76	Abudabi	$2a$10$kaeV6DmjnvMQVUh5eZHtcOgJCLxGxHFIap2nK.xCGS6OEUSnzCk3W	arar@aaasr.abudabi	\N	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	123123213	0	f	0	f	f
77	Люцифер	$2a$10$L9FsmB/kI1nR2VCnsBdIZ.wY3bd763UIH3tkQzUNwQ4sWVdLvsS9G	abudabi@asd.ru	\N	{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}	Цинис	0	f	0	f	f
\.


--
-- Name: banners banners_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY banners
    ADD CONSTRAINT banners_pkey PRIMARY KEY (banner_id);


--
-- Name: colors colors_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY colors
    ADD CONSTRAINT colors_pkey PRIMARY KEY (color_id);


--
-- Name: incexps incexps_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY incexps
    ADD CONSTRAINT incexps_pkey PRIMARY KEY (incexp_id);


--
-- Name: lists lists_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY lists
    ADD CONSTRAINT lists_pkey PRIMARY KEY (list_id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (order_id);


--
-- Name: positions positions_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY positions
    ADD CONSTRAINT positions_pkey PRIMARY KEY ("position");


--
-- Name: products products_article_number_key; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_article_number_key UNIQUE (article_number);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (product_id);


--
-- Name: promos promos_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY promos
    ADD CONSTRAINT promos_pkey PRIMARY KEY (promo_id);


--
-- Name: statuses statuses_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY statuses
    ADD CONSTRAINT statuses_pkey PRIMARY KEY (status_id);


--
-- Name: types types_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY types
    ADD CONSTRAINT types_pkey PRIMARY KEY (type_id);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: j_uuid_idx; Type: INDEX; Schema: public; Owner: dmd_fs_user
--

CREATE UNIQUE INDEX j_uuid_idx ON products USING btree ((((reviews ->> 'user_id'::text))::integer));


--
-- Name: name_index; Type: INDEX; Schema: public; Owner: dmd_fs_user
--

CREATE UNIQUE INDEX name_index ON promos USING btree (name);


--
-- Name: orders NewSuccessOrder; Type: TRIGGER; Schema: public; Owner: dmd_fs_user
--

CREATE TRIGGER "NewSuccessOrder" AFTER UPDATE ON orders FOR EACH ROW EXECUTE PROCEDURE orders_count_recalc();


--
-- Name: products sizes_count_recalc; Type: TRIGGER; Schema: public; Owner: dmd_fs_user
--

CREATE TRIGGER sizes_count_recalc AFTER UPDATE OF sizes ON products FOR EACH ROW EXECUTE PROCEDURE sizes_count_recalc();


--
-- Name: orders promo_id; Type: FK CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT promo_id FOREIGN KEY (promo_id) REFERENCES promos(promo_id);


--
-- Name: orders status_id; Type: FK CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT status_id FOREIGN KEY (status_id) REFERENCES statuses(status_id);


--
-- Name: orders user_id; Type: FK CONSTRAINT; Schema: public; Owner: dmd_fs_user
--

ALTER TABLE ONLY orders
    ADD CONSTRAINT user_id FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- PostgreSQL database dump complete
--

