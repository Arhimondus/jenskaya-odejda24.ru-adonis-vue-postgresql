/*
Navicat PGSQL Data Transfer

Source Server         : dmd_remote
Source Server Version : 90602
Source Host           : 185.58.204.22:5432
Source Database       : dmd_dev
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90602
File Encoding         : 65001

Date: 2017-09-03 00:29:50
*/


-- ----------------------------
-- Sequence structure for banner_autoid
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."banner_autoid";
CREATE SEQUENCE "public"."banner_autoid"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 9
 CACHE 1;
SELECT setval('"public"."banner_autoid"', 9, true);

-- ----------------------------
-- Sequence structure for incexp_autoid
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."incexp_autoid";
CREATE SEQUENCE "public"."incexp_autoid"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 16
 CACHE 1;
SELECT setval('"public"."incexp_autoid"', 16, true);

-- ----------------------------
-- Sequence structure for list_autoid
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."list_autoid";
CREATE SEQUENCE "public"."list_autoid"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 29
 CACHE 1;
SELECT setval('"public"."list_autoid"', 29, true);

-- ----------------------------
-- Sequence structure for order_autoid
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."order_autoid";
CREATE SEQUENCE "public"."order_autoid"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 40
 CACHE 1;
SELECT setval('"public"."order_autoid"', 40, true);

-- ----------------------------
-- Sequence structure for product_autoid
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."product_autoid";
CREATE SEQUENCE "public"."product_autoid"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 63
 CACHE 1;
SELECT setval('"public"."product_autoid"', 63, true);

-- ----------------------------
-- Sequence structure for promo_autoid
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."promo_autoid";
CREATE SEQUENCE "public"."promo_autoid"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 8
 CACHE 1;
SELECT setval('"public"."promo_autoid"', 8, true);

-- ----------------------------
-- Sequence structure for user_autoid
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."user_autoid";
CREATE SEQUENCE "public"."user_autoid"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 78
 CACHE 1;
SELECT setval('"public"."user_autoid"', 78, true);

-- ----------------------------
-- Table structure for banners
-- ----------------------------
DROP TABLE IF EXISTS "public"."banners";
CREATE TABLE "public"."banners" (
"banner_id" int4 DEFAULT nextval('banner_autoid'::regclass) NOT NULL,
"uri" varchar(255) COLLATE "default" NOT NULL,
"alt" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default" DEFAULT '[Без названия]'::character varying,
"image" varchar(255) COLLATE "default",
"position" varchar(255) COLLATE "default",
"order" int2 DEFAULT 0 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of banners
-- ----------------------------
INSERT INTO "public"."banners" VALUES ('1', 'banner_zlo_uri', 'альтернативное написание', '260xN', 'Sk2Jg7PKW', 'list', '0');
INSERT INTO "public"."banners" VALUES ('2', 'rubashki-v-kletku', '324234', '260xN', 'BJuylmDKb', 'list', '0');
INSERT INTO "public"."banners" VALUES ('3', '/dzhinsi', '32523', 'Верхний 370x180', 'H1GWBfwYZ', 'main_top', '3');
INSERT INTO "public"."banners" VALUES ('4', 'rubashki', '234234', 'Верхний 370x180', 'BkKTHGwK-', 'main_top', '2');
INSERT INTO "public"."banners" VALUES ('5', 'https://ya.ru', '234234', 'Верхний 750x390', 'BkWZCGDKZ', 'main_top', '1');
INSERT INTO "public"."banners" VALUES ('6', 'Баннер снизу', 'Баннер снизу', 'Нижний 420x180', 'ryagpGDF-', 'main_bottom', '1');
INSERT INTO "public"."banners" VALUES ('7', '23423', '423423', 'Нижний 420x180', 'HJJ46MPFW', 'main_bottom', '2');
INSERT INTO "public"."banners" VALUES ('8', '2342423', '2342423', 'Нижний 720x307', 'rJRN6fDtZ', 'main_bottom', '3');
INSERT INTO "public"."banners" VALUES ('9', '114', '124124', 'Нижний 370x538', 'S1RSafvKb', 'main_bottom', '4');

-- ----------------------------
-- Table structure for colors
-- ----------------------------
DROP TABLE IF EXISTS "public"."colors";
CREATE TABLE "public"."colors" (
"color_id" int4 NOT NULL,
"name" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of colors
-- ----------------------------
INSERT INTO "public"."colors" VALUES ('1', 'Белый');
INSERT INTO "public"."colors" VALUES ('2', 'Чёрный');
INSERT INTO "public"."colors" VALUES ('3', 'Красный');
INSERT INTO "public"."colors" VALUES ('4', 'Синий');

-- ----------------------------
-- Table structure for incexps
-- ----------------------------
DROP TABLE IF EXISTS "public"."incexps";
CREATE TABLE "public"."incexps" (
"incexp_id" int4 DEFAULT nextval('incexp_autoid'::regclass) NOT NULL,
"changes" jsonb,
"product_id" int4,
"comment" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default",
"date" date DEFAULT now(),
"type" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of incexps
-- ----------------------------
INSERT INTO "public"."incexps" VALUES ('2', '{"42": 5, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'expense');
INSERT INTO "public"."incexps" VALUES ('3', '{"42": 5, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'income');
INSERT INTO "public"."incexps" VALUES ('4', '{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 1, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'income');
INSERT INTO "public"."incexps" VALUES ('5', '{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 1}', '60', null, 'Аделия Рубашка', '2017-08-27', 'income');
INSERT INTO "public"."incexps" VALUES ('6', '{"42": 1, "44": 1, "46": 1, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'income');
INSERT INTO "public"."incexps" VALUES ('7', '{"42": 2, "44": 4, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'expense');
INSERT INTO "public"."incexps" VALUES ('8', '{"42": 0, "44": 0, "46": 5, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'expense');
INSERT INTO "public"."incexps" VALUES ('9', '{"42": 0, "44": 0, "46": 0, "48": 3, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'expense');
INSERT INTO "public"."incexps" VALUES ('10', '{"42": 0, "44": 0, "46": 0, "48": 1, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'expense');
INSERT INTO "public"."incexps" VALUES ('11', '{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 1, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'expense');
INSERT INTO "public"."incexps" VALUES ('12', '{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 8}', '60', null, 'Аделия Рубашка', '2017-08-27', 'income');
INSERT INTO "public"."incexps" VALUES ('13', '{"42": 5, "44": 2, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'income');
INSERT INTO "public"."incexps" VALUES ('14', '{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 15}', '60', null, 'Аделия Рубашка', '2017-08-27', 'income');
INSERT INTO "public"."incexps" VALUES ('15', '{"42": 0, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 3, "68": 0}', '60', null, 'Аделия Рубашка', '2017-08-27', 'income');
INSERT INTO "public"."incexps" VALUES ('16', '{"42": 0, "44": 0, "46": 0, "48": 5, "50": 5, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '62', null, 'Джинсы длины 7/8 с принтом', '2017-08-28', 'income');

-- ----------------------------
-- Table structure for lists
-- ----------------------------
DROP TABLE IF EXISTS "public"."lists";
CREATE TABLE "public"."lists" (
"list_id" int4 DEFAULT nextval('list_autoid'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default",
"title" varchar(255) COLLATE "default",
"uri" varchar(255) COLLATE "default",
"order" int4 DEFAULT 0,
"color" varchar(255) COLLATE "default",
"in_menu" bool NOT NULL,
"max_size" int2,
"type_id" int2,
"catalog_title" varchar(255) COLLATE "default",
"column" int2,
"header_bg" varchar COLLATE "default",
"in_catalog" bool NOT NULL,
"in_main" bool NOT NULL,
"meta_description" varchar(255) COLLATE "default",
"meta_keywords" varchar(255) COLLATE "default",
"parent_id" int4,
"articles" json
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of lists
-- ----------------------------
INSERT INTO "public"."lists" VALUES ('17', 'Блузки', 'Купить блузки', 'bluzki', '0', '', 'f', null, '2', 'Каталог блузок', '1', 'rJG7xATOW', 't', 'f', 'описание', 'блузки хорошие, блузки отличные', null, '[{"header":"Суперзаголовок мега плюс XXL","content":"235235","tab":"Вкладка"},{"header":"Заголовок2","content":"235235","tab":"Вкладка"},{"header":"Суперзаголовок мега плюс XXL","content":"23523523","tab":"Вкладка"},{"header":"Заголовок4","content":"235235","tab":"Вкладка"},{"header":"Заголовок5","content":"dfgdfg","tab":"Вкладка"}]');
INSERT INTO "public"."lists" VALUES ('24', 'Рубашки', '123', 'rubashki', '1', null, 'f', '4', '2', 'Каталог рубашек', '1', 'ryOpZJRO-', 't', 'f', '123', '123', null, '[{"header":"how to reliably","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"how to reliably"},{"header":"how to reliably","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"how to reliably"},{"header":"how to reliably","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"how to reliably"},{"header":"how to reliably","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"how to reliably"},{"header":"234","content":"vaScript can often be a pain, especially for new JS developers. I want to show you how to reliably check types in JS and understand them a little more. This post digs through Objects, Primitives, shadow objects/coercion, the typeof operator and how we can reliably get a “real” JavaScript type.","tab":"23"}]');
INSERT INTO "public"."lists" VALUES ('25', 'Рубашки в клетку', 'впрв', 'rubashki-v-kletku', '2', null, 'f', null, null, 'Рубашки в клетку с доставкой', null, 'S1DJXy0uW', 'f', 'f', 'орпаро', 'апропаро', '24', '[{"header":"fccvv","content":"fccvv","tab":"sdfsd"},{"header":"fccvvfccvv","content":"fccvv","tab":"fccvv"},{"header":"fccvv","content":"fccvv","tab":"fccvv"},{"header":"fccvvfccvvfccvv","content":"fccvvfccvvfccvvfccvv","tab":"fccvvfccvv"},{"header":"fccvv","content":"fccvvfccvv","tab":"fccvv"}]');
INSERT INTO "public"."lists" VALUES ('26', 'Рубашки белые', '2554545', 'rubashki-belie', '1', null, 'f', null, null, 'Офисные белые рубашки', null, 'B1udLJAu-', 'f', 'f', '3вапвапвап', '2353255', '24', '[{"header":"234324","content":"ё","tab":"Статья"},{"header":"","content":"","tab":"Статья2"},{"header":"","content":"","tab":"Статья2"},{"header":"","content":"","tab":"Статья2"},{"header":"124","content":"214","tab":"Статья9"}]');
INSERT INTO "public"."lists" VALUES ('27', 'Кофты', 'кофты', 'kofti', '3', null, 'f', null, '2', 'Каталог кофт', '2', 'B1xovkRdb', 't', 'f', 'кофты описание', 'кофты хорошие', null, '[{"header":"стат1","content":"стат1","tab":"стат1"},{"header":"стат1","content":"стат1","tab":"стат1"},{"header":"стат1","content":"стат1","tab":"стат1"},{"header":"стат1","content":"стат1","tab":"стат1"},{"header":"стат1","content":"стат1","tab":"стат1"}]');
INSERT INTO "public"."lists" VALUES ('28', 'Главная', 'Главная', '', null, null, 'f', null, null, 'Главная', null, null, 'f', 't', 'Главная', 'Главная', null, '[{"header":"2Женщина3234","content":"В интернет-магазине Lamoda.ru представлены лучшие коллекции женской одежды, обуви и аксессуаров, а также косметики и парфюмов ведущих мировых брендов.\nУвлечённые модницы могут выбрать товары премиум-класса и органично совместить их с базовыми вещами демократичной ценовой категории. Любая п","tab":"Женщинам"},{"header":"3Мужчинам45","content":"В интернет-магазине Lamoda.ru представлены лучшие коллекции женской одежды, обуви и аксессуаров, а также косметики и парфюмов ведущих мировых брендов.\nУвлечённые модницы могут выбрать товары премиум-класса и органично совместить их с базовыми вещами демократичной ценовой категории. Любая п","tab":"Мужчинам"},{"header":"34534","content":"В интернет-магазине Lamoda.ru представлены лучшие коллекции женской одежды, обуви и аксессуаров, а также косметики и парфюмов ведущих мировых брендов.\nУвлечённые модницы могут выбрать товары премиум-класса и органично совместить их с базовыми вещами демократичной ценовой категории. Любая п","tab":"Сумкам"},{"header":"Блузхкам","content":"В интернет-магазине Lamoda.ru представлены лучшие коллекции женской одежды, обуви и аксессуаров, а также косметики и парфюмов ведущих мировых брендов.\nУвлечённые модницы могут выбрать товары премиум-класса и органично совместить их с базовыми вещами демократичной ценовой категории. Любая п","tab":"Блузхкам"},{"header":"3Юбкам45","content":"Интернет-магазин женской одежды, обуви, аксессуаров, косметики и парфюмерии","tab":"Юбкам"}]');
INSERT INTO "public"."lists" VALUES ('29', 'Новинки', 'Новинки', 'novinki', '1', null, 't', null, null, 'Новинки', null, 'S1lEqGwtZ', 'f', 'f', 'Новинки', 'Новинки', null, '[{"header":"","content":"","tab":""},{"header":"","content":"","tab":""},{"header":"","content":"","tab":""},{"header":"","content":"","tab":""},{"header":"","content":"","tab":""}]');

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS "public"."orders";
CREATE TABLE "public"."orders" (
"order_id" int4 DEFAULT nextval('order_autoid'::regclass) NOT NULL,
"user_id" int4,
"items" jsonb NOT NULL,
"address" jsonb NOT NULL,
"personal" jsonb NOT NULL,
"comment" varchar(255) COLLATE "default",
"datetime" timestamp(0) DEFAULT now() NOT NULL,
"summary" int4 NOT NULL,
"delivery" int4 NOT NULL,
"total" int4 NOT NULL,
"status_id" int2 DEFAULT 1 NOT NULL,
"promo" varchar(255) COLLATE "default",
"promo_id" int4,
"bonuses" int2,
"accrued" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO "public"."orders" VALUES ('17', '1', '[{"name": "Аделия Рубашка", "size": "64", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1143}, {"name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 745}]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-08-29 15:34:26', '1888', '250', '1194', '4', null, null, null, null);
INSERT INTO "public"."orders" VALUES ('18', '1', '[{"name": "Аделия Рубашка", "size": "68", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1143}, {"name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 745}]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-08-29 15:40:20', '1888', '250', '1194', '3', null, null, null, null);
INSERT INTO "public"."orders" VALUES ('19', '1', '[{"name": "Аделия Рубашка", "size": "68", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1875}, {"name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1875}, {"name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1875}]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-08-29 15:40:52', '5625', '0', '3125', '4', null, null, null, null);
INSERT INTO "public"."orders" VALUES ('20', '1', '[{"name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1143}, {"name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 745}, {"name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 745}, {"name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 745}]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-08-29 15:59:46', '3378', '0', '1689', '4', null, null, null, null);
INSERT INTO "public"."orders" VALUES ('21', '1', '[{"name": "Аделия Рубашка", "size": "44", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1593}, {"name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 1038}]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-08-29 16:31:48', '2631', '0', '1316', '4', 'PROMOCODE2017', '7', '1315', null);
INSERT INTO "public"."orders" VALUES ('22', '1', '[{"name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"name": "Аделия Рубашка", "size": "64", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"name": "Аделия Рубашка", "size": "44", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-08-29 18:55:02', '3000', '250', '1750', '3', null, null, '1500', null);
INSERT INTO "public"."orders" VALUES ('23', '1', '[{"id": 1, "name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 1222}]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-08-31 00:57:00', '1222', '250', '1472', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('24', null, '[{"id": 1, "name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 1222}, {"id": 2, "name": "Аделия Рубашка", "size": "64", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"id": 3, "name": "Джинсы длины 7/8 с принтом", "size": "50", "product_id": 62, "article_number": "уае642", "computed_price": 1222}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "214", "email": "artemiytm@yandex.ru", "phone": "24"}', null, '2017-08-31 01:33:20', '3444', '0', '3444', '2', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('25', null, '[{"id": 1, "name": "Джинсы модные", "size": "44", "product_id": 63, "article_number": "432", "computed_price": 5173}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "rr", "email": "artemiy@a.rr", "phone": "r"}', null, '2017-08-31 02:06:20', '5173', '0', '5173', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('26', null, '[{"id": 1, "name": "Джинсы модные", "size": "64", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 2, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "Abudabi", "email": "arar@aaasr.abudabi", "phone": "123123213"}', null, '2017-08-31 02:16:40', '6173', '0', '6173', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('27', null, '[{"id": 1, "name": "Джинсы модные", "size": "44", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 2, "name": "Аделия Рубашка", "size": "66", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:21:46', '6173', '0', '6173', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('28', null, '[{"id": 1, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"id": 2, "name": "Джинсы длины 7/8 с принтом", "size": "48", "product_id": 62, "article_number": "уае642", "computed_price": 1222}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "Архонт", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:27:04', '2222', '250', '2472', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('29', null, '[{"id": 1, "name": "Джинсы модные", "size": "66", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 2, "name": "Джинсы модные", "size": "66", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 3, "name": "Джинсы модные", "size": "66", "product_id": 63, "article_number": "432", "computed_price": 5173}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:32:45', '15519', '0', '15519', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('30', null, '[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:34:57', '5173', '0', '5173', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('31', null, '[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:38:23', '5173', '0', '5173', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('32', null, '[{"id": 1, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:40:06', '1000', '250', '1250', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('33', null, '[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}]', '{"city": "343", "code": "234", "flat": "124124124", "house": "34134", "porch": "234", "street": "234", "building": "324"}', '{"name": "sdfsfsd", "email": "abudabi@asd.ru", "phone": "234234"}', null, '2017-08-31 02:42:39', '5173', '0', '5173', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('34', null, '[{"id": 1, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"id": 2, "name": "Аделия Рубашка", "size": "66", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}, {"id": 3, "name": "Аделия Рубашка", "size": "66", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:44:46', '3000', '0', '3000', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('35', '77', '[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}, {"id": 2, "name": "Аделия Рубашка", "size": "42", "product_id": 60, "article_number": "ФАфыа23", "computed_price": 1000}]', '{"city": "", "code": "", "flat": "214124", "house": "124", "porch": "124", "street": "", "building": ""}', '{"name": "", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:49:04', '6173', '0', '6173', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('36', '77', '[{"id": 1, "name": "Джинсы модные", "size": "68", "product_id": 63, "article_number": "432", "computed_price": 5173}]', '{"city": "", "code": "", "flat": "24", "house": "", "porch": "", "street": "", "building": ""}', '{"name": "", "email": "abudabi@asd.ru", "phone": ""}', null, '2017-08-31 02:53:14', '5173', '0', '5173', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('37', '1', '[]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-09-01 13:59:46', '0', '250', '250', '3', null, null, '0', null);
INSERT INTO "public"."orders" VALUES ('38', '1', '[]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Маргарита333", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-09-01 14:14:14', '0', '250', '250', '3', 'ZIMA2018', '8', '0', null);
INSERT INTO "public"."orders" VALUES ('39', '1', '[]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Марга33рита", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', null, '2017-09-01 14:41:01', '3155', '0', '3155', '3', 'ZIMA2018', '8', '0', null);
INSERT INTO "public"."orders" VALUES ('40', '1', '[]', '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '{"name": "Марга3353", "email": "artemiytm@ya.ru", "phone": "+79057641220"}', '1241212412', '2017-09-01 18:33:38', '0', '250', '250', '3', null, null, '0', null);

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS "public"."positions";
CREATE TABLE "public"."positions" (
"position" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of positions
-- ----------------------------
INSERT INTO "public"."positions" VALUES ('list');
INSERT INTO "public"."positions" VALUES ('main_bottom');
INSERT INTO "public"."positions" VALUES ('main_top');

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS "public"."products";
CREATE TABLE "public"."products" (
"product_id" int4 DEFAULT nextval('product_autoid'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"seo_uri" varchar(255) COLLATE "default",
"description" text COLLATE "default" DEFAULT ''::character varying NOT NULL,
"price" int2 NOT NULL,
"base_price" int2,
"sizes" jsonb,
"color_id" int2,
"images" jsonb DEFAULT '["no-image"]'::jsonb,
"tag" varchar(255) COLLATE "default",
"lists" jsonb,
"fields" jsonb DEFAULT '[]'::jsonb,
"reviews" jsonb DEFAULT '[]'::jsonb,
"questions" jsonb DEFAULT '[]'::jsonb,
"special" varchar(255) COLLATE "default",
"count_free" int2,
"article_number" varchar(255) COLLATE "default" NOT NULL,
"type_id" int4 NOT NULL,
"discount" int2,
"count" int2 DEFAULT 0
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO "public"."products" VALUES ('60', 'Аделия Рубашка', null, 'выаапп вапав пвап авп вап вап вап  вап вап вапвыаапп вапав пвап авп вап вап вап  вап вап вапвыаапп вапав п <a href="/">adadasd ссылка </a>вап авп вап вап вап  вап вап вапвыаапп вапав пвап авп вап вап вап  вап вап вапвыаапп вапав пвап авп вап вап вап  вап вап вап3434', '1325', '2500', '{"42": 5, "44": 0, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 3, "68": 24}', '3', '["ryDVwgRdZ", "HJu4vx0d-", "SklO4DeRd-", "BkK4DlRuZ", "Sy5NwlAOZ"]', 'NEW', '[24, 25, 27, 28]', '[{"name": "Длина", "value": "456"}, {"name": "Длина", "value": "456"}, {"name": "Длина", "value": "3"}, {"name": "Длина", "value": "34"}, {"name": "Длина", "value": "34"}, {"name": "Длина", "value": "34"}, {"name": "Длина", "value": "53"}, {"name": "Длина", "value": "345345345345345345"}]', '[{"date": "2017-08-29", "name": "124124", "rate": 5, "text": "124", "user_id": 1}, {"date": "2017-08-29", "name": "Экимора", "rate": 3, "text": "1123123", "user_id": 1}, {"date": "2017-08-29", "name": "Левиафан", "rate": 1, "text": "Просто отвратительный товар!", "user_id": 1}]', '[{"date": "2017-08-29", "name": "Лейкемия", "text": "Мой вопрос. Почему это отвартительнейший товар ещё здесь????\n", "user_id": 1}]', null, '3', 'ФАфыа23', '2', '47', '32');
INSERT INTO "public"."products" VALUES ('62', 'Джинсы длины 7/8 с принтом', null, 'ender API — это специальный интерфейс для разработчиков, позволяющий интегрировать возможности электронной рассылки практически с любым открытым веб-сервисом или desktop-приложением. Наш API предоставляет возможности для управления списками подписчиков, создания и отправки разных типов сообщений, получения доступа к статистике, а также для работы партнёров.', '11837', '13300', '{"42": 0, "44": 0, "46": 0, "48": 3, "50": 4, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0}', '2', '["HJ6QucZFW", "rJJVucbYW", "ryxNdcWtZ"]', 'ДА!', '[28]', '[{"name": "Длина", "value": "Великая"}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}]', '[{"date": "2017-08-29", "name": "234324", "rate": 1, "text": "234234", "user_id": 1}]', '[]', null, '3', 'уае642', '3', '11', '7');
INSERT INTO "public"."products" VALUES ('63', 'Джинсы модные', null, 'Джинсы модные', '4799', '5333', '{"42": 0, "44": 1, "46": 0, "48": 0, "50": 0, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 1, "66": 3, "68": 24}', '1', '["rJMjoxXK-"]', '4', '[17, 24, 27]', '[{"name": "35", "value": "35"}, {"name": "355", "value": "353"}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}, {"name": "", "value": ""}]', '[]', '[]', null, '2', '432', '2', '10', '29');

-- ----------------------------
-- Table structure for promos
-- ----------------------------
DROP TABLE IF EXISTS "public"."promos";
CREATE TABLE "public"."promos" (
"promo_id" int4 DEFAULT nextval('promo_autoid'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"discount" int2 DEFAULT 5 NOT NULL,
"last_date" date,
"enabled" bool DEFAULT true NOT NULL,
"single_use" bool NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of promos
-- ----------------------------
INSERT INTO "public"."promos" VALUES ('7', 'PROMOCODE2017', '15', null, 't', 't');
INSERT INTO "public"."promos" VALUES ('8', 'ZIMA2018', '39', '2017-09-16', 't', 'f');

-- ----------------------------
-- Table structure for sizes
-- ----------------------------
DROP TABLE IF EXISTS "public"."sizes";
CREATE TABLE "public"."sizes" (
"sizes" jsonb
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sizes
-- ----------------------------
INSERT INTO "public"."sizes" VALUES ('{"42": 0, "44": 0, "46": 0, "48": 5, "50": 5, "52": 0, "54": 0, "56": 0, "58": 0, "60": 0, "62": 0, "64": 0, "66": 0, "68": 0, "[object Object]": 0}');

-- ----------------------------
-- Table structure for statuses
-- ----------------------------
DROP TABLE IF EXISTS "public"."statuses";
CREATE TABLE "public"."statuses" (
"status_id" int4 NOT NULL,
"name" varchar(255) COLLATE "default" NOT NULL,
"color" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of statuses
-- ----------------------------
INSERT INTO "public"."statuses" VALUES ('1', 'Новый', '#4ac7ac');
INSERT INTO "public"."statuses" VALUES ('2', 'В процессе', 'rgb(255, 123, 159)');
INSERT INTO "public"."statuses" VALUES ('3', 'Отменён', 'rgba(179, 179, 179, 0.75)');
INSERT INTO "public"."statuses" VALUES ('4', 'Завершён', 'rgb(84, 181, 60)');
INSERT INTO "public"."statuses" VALUES ('5', 'Возращённый', '#7eb3b0');

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS "public"."types";
CREATE TABLE "public"."types" (
"type_id" int4 NOT NULL,
"sizes" jsonb,
"name" varchar(255) COLLATE "default",
"size_table" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO "public"."types" VALUES ('1', '[{"name": "Безразмерный", "size": "∞"}]', 'Безразмерный', null);
INSERT INTO "public"."types" VALUES ('2', '[{"name": "42 / 44 RUS", "size": "42"}, {"name": "44 / 46 RUS", "size": "44"}, {"name": "46 / 48 RUS", "size": "46"}, {"name": "48 / 50 RUS", "size": "48"}, {"name": "50 / 52 RUS", "size": "50"}, {"name": "52 / 54 RUS", "size": "52"}, {"name": "54 / 56 RUS", "size": "54"}, {"name": "56 / 58 RUS", "size": "56"}, {"name": "58 / 60 RUS", "size": "58"}, {"name": "60 / 62 RUS", "size": "60"}, {"name": "62 / 64 RUS", "size": "62"}, {"name": "64 / 66 RUS", "size": "64"}, {"name": "66 / 68 RUS", "size": "66"}, {"name": "68 / 70 RUS", "size": "68"}]', 'Одежда-верх', '<table class="size-table">
	<tbody>
		<tr>
			<td class="size-table__left-align size-table__bold">Обхват груди (см)</td>
			<td>82-86</td>
			<td>86-90</td>
			<td>90-94</td>
			<td>94-98</td>
			<td>98-102</td>
			<td>102-106</td>
			<td>106-110</td>
			<td>110-114</td>
			<td>114-118</td> <!--58-->
			<td>118-122</td>
			<td>122-126</td>
			<td>126-130</td>
			<td>130-134</td>
			<td>134-138</td>
		</tr>
		<tr>
			<td class="size-table__left-align size-table__bold">Обхват талии (см)</td>
			<td>62-66</td>
			<td>66-70</td>
			<td>70-74</td>
			<td>74-78</td>
			<td>78-82</td>
			<td>82-86</td>
			<td>86-90</td>
			<td>90-95</td>
			<td>95-100</td> <!--58-->
			<td>100-105</td>
			<td>105-110</td>
			<td>110-115</td>
			<td>115-120</td>
			<td>120-125</td>
		</tr>
		<tr class="size-table__bold">
			<td class="size-table__left-align">Российский размер (RUS)</td>
			<td>42</td>
			<td>44</td>
			<td>46</td>
			<td>48</td>
			<td>50</td>
			<td>52</td>
			<td>54</td>
			<td>56</td>
			<td>58</td> <!--58-->
			<td>60</td>
			<td>62</td>
			<td>64</td>
			<td>66</td>
			<td>68</td>
		</tr>
		<tr>
			<td class="size-table__left-align  size-table__bold">Китайский</td>
			<td>M</td>
			<td>L</td>
			<td>L</td>
			<td>XL</td>
			<td>2XL</td>
			<td>2XL</td>
			<td>3XL</td>
			<td>3XL</td>
			<td>4XL</td>  <!--58-->
			<td>4XL</td>
			<td>5XL</td>
			<td>5XL</td>
			<td>5XL</td>
			<td>5XL</td>
		</tr>
		<tr>
			<td class="size-table__left-align size-table__bold">Европа &nbsp;(EUR)</td>
			<td>34</td>
			<td>36</td>
			<td>38</td>
			<td>40</td>
			<td>42</td>
			<td>44</td>
			<td>46</td>
			<td>48</td>
			<td>50</td>  <!--58-->
			<td>52</td>
			<td>54</td>
			<td>56</td>
			<td>58</td>
			<td>60</td>
		</tr>
		<tr>
			<td class="size-table__left-align size-table__bold">США (USA)</td>
			<td>4</td>
			<td>6</td>
			<td>8</td>
			<td>10</td>
			<td>12</td>
			<td>14</td>
			<td>16</td>
			<td>18</td>  
			<td>20</td> <!--58-->
			<td>22</td>
			<td>24</td>
			<td>26</td>
			<td>28</td>
			<td>30</td>
		</tr>
		<tr>
			<td class="size-table__left-align size-table__bold">Международный (INT)</td>
			<td>XS</td>
			<td>S</td>
			<td>M</td>
			<td>M</td>
			<td>L</td>
			<td>XL</td>
			<td>XL</td>
			<td>2XL</td>
			<td>3XL</td> <!--58-->
			<td>4XL</td>
			<td>5XL</td>
			<td>6XL</td>
			<td>7XL</td>
			<td>8XL</td>
		</tr>
	</tbody>
</table>');
INSERT INTO "public"."types" VALUES ('3', '[{"name": "42 / 44 RUS", "size": "42"}, {"name": "44 / 46 RUS", "size": "44"}, {"name": "46 / 48 RUS", "size": "46"}, {"name": "48 / 50 RUS", "size": "48"}, {"name": "50 / 52 RUS", "size": "50"}, {"name": "52 / 54 RUS", "size": "52"}, {"name": "54 / 56 RUS", "size": "54"}, {"name": "56 / 58 RUS", "size": "56"}, {"name": "58 / 60 RUS", "size": "58"}, {"name": "60 / 62 RUS", "size": "60"}, {"name": "62 / 64 RUS", "size": "62"}, {"name": "64 / 66 RUS", "size": "64"}, {"name": "66 / 68 RUS", "size": "66"}, {"name": "68 / 70 RUS", "size": "68"}]', 'Одежда-низ', '<table class="size-table">
	<tbody>
		<tr>
			<td class="size-table__left-align size-table__bold">Обхват бёдер (см)</td>
			<td>88-92</td>
			<td>92-96</td>
			<td>96-100</td>
			<td>100-104</td>
			<td>104-108</td>
			<td>108-112</td>
			<td>112-116</td>
			<td>116-120</td>
			<td>120-124</td> <!--58-->
			<td>124-128</td>
			<td>128-132</td>
			<td>132-136</td>
			<td>136-140</td>
			<td>140-144</td>
		</tr>
		<tr>
			<td class="size-table__left-align size-table__bold">Обхват талии (см)</td>
			<td>62-66</td>
			<td>66-70</td>
			<td>70-74</td>
			<td>74-78</td>
			<td>78-82</td>
			<td>82-86</td>
			<td>86-90</td>
			<td>90-95</td>
			<td>95-100</td> <!--58-->
			<td>100-105</td>
			<td>105-110</td>
			<td>110-115</td>
			<td>115-120</td>
			<td>120-125</td>
		</tr>
		<tr class="size-table__bold">
			<td class="size-table__left-align">Российский размер (RUS)</td>
			<td>42</td>
			<td>44</td>
			<td>46</td>
			<td>48</td>
			<td>50</td>
			<td>52</td>
			<td>54</td>
			<td>56</td>
			<td>58</td> <!--58-->
			<td>60</td>
			<td>62</td>
			<td>64</td>
			<td>66</td>
			<td>68</td>
		</tr>
		<tr>
			<td class="size-table__left-align  size-table__bold">Китайский</td>
			<td>M</td>
			<td>L</td>
			<td>L</td>
			<td>XL</td>
			<td>2XL</td>
			<td>2XL</td>
			<td>3XL</td>
			<td>3XL</td>
			<td>4XL</td>  <!--58-->
			<td>4XL</td>
			<td>5XL</td>
			<td>5XL</td>
			<td>5XL</td>
			<td>5XL</td>
		</tr>
		<tr>
			<td class="size-table__left-align size-table__bold">Европа (EUR)</td>
			<td>34</td>
			<td>36</td>
			<td>38</td>
			<td>40</td>
			<td>42</td>
			<td>44</td>
			<td>46</td>
			<td>48</td>
			<td>50</td>  <!--58-->
			<td>52</td>
			<td>54</td>
			<td>56</td>
			<td>58</td>
			<td>60</td>
		</tr>
		<tr>
			<td class="size-table__left-align size-table__bold">США (USA)</td>
			<td>4</td>
			<td>6</td>
			<td>8</td>
			<td>10</td>
			<td>12</td>
			<td>14</td>
			<td>16</td>
			<td>18</td>  
			<td>20</td> <!--58-->
			<td>22</td>
			<td>24</td>
			<td>26</td>
			<td>28</td>
			<td>30</td>
		</tr>
		<tr>
			<td class="size-table__left-align size-table__bold">Международный (INT)</td>
			<td>XS</td>
			<td>S</td>
			<td>M</td>
			<td>M</td>
			<td>L</td>
			<td>XL</td>
			<td>XL</td>
			<td>2XL</td>
			<td>3XL</td> <!--58-->
			<td>4XL</td>
			<td>5XL</td>
			<td>6XL</td>
			<td>7XL</td>
			<td>8XL</td>
		</tr>
	</tbody>
</table>');
INSERT INTO "public"."types" VALUES ('4', '[{"name": "34 RUS", "size": "34"}, {"name": "34.5 RUS", "size": "34.5"}, {"name": "35 RUS", "size": "35"}, {"name": "36 RUS", "size": "36"}, {"name": "37 RUS", "size": "37"}, {"name": "37.5 RUS", "size": "37.5"}, {"name": "38 RUS", "size": "38"}, {"name": "38.5 RUS", "size": "38.5"}, {"name": "39 RUS", "size": "39"}, {"name": "40 RUS", "size": "40"}, {"name": "40.5 RUS", "size": "40.5"}, {"name": "41 RUS", "size": "41"}, {"name": "42 RUS", "size": "42"}, {"name": "43 RUS", "size": "43"}]', 'Обувь', null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
"user_id" int4 DEFAULT nextval('user_autoid'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"password" varchar(255) COLLATE "default" NOT NULL,
"email" varchar(255) COLLATE "default" NOT NULL,
"activation_code" varchar(255) COLLATE "default",
"address" jsonb DEFAULT '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}'::jsonb,
"phone" varchar(255) COLLATE "default" DEFAULT ''::character varying,
"bonuses" int2 DEFAULT 0 NOT NULL,
"is_admin" bool DEFAULT false NOT NULL,
"orders" int2 DEFAULT 0 NOT NULL,
"sms_subscribe" bool DEFAULT false,
"email_subscribe" bool DEFAULT false
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES ('1', 'Маргарита', '$2a$10$xXH1/v4J6YMjv1iQZWYGveBVbccUX7CEM2J4ab7XLbTnxUm5OD2eS', 'artemiytm@ya.ru', null, '{"city": "г. Армагеддог4434", "code": "34", "flat": "234", "house": "234234", "porch": "234", "street": "Армагеддонович", "building": "324234"}', '+79057641220', '0', 'f', '4', 'f', 't');
INSERT INTO "public"."users" VALUES ('74', '', '$2a$10$vA/eAVgyL0J6P/baniW01.qW0fs7YHWiqwq1ozsv5ukSFGV42koqy', 'robot@gmail.com', 'rJwAyCR_b', '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '', '-2500', 'f', '0', 'f', 'f');
INSERT INTO "public"."users" VALUES ('75', 'rr', 'Hy7xB6EKW', 'artemiy@a.rr', null, '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', 'r', '0', 'f', '0', 'f', 'f');
INSERT INTO "public"."users" VALUES ('76', 'Abudabi', '$2a$10$kaeV6DmjnvMQVUh5eZHtcOgJCLxGxHFIap2nK.xCGS6OEUSnzCk3W', 'arar@aaasr.abudabi', null, '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', '123123213', '0', 'f', '0', 'f', 'f');
INSERT INTO "public"."users" VALUES ('77', 'Люцифер', '$2a$10$L9FsmB/kI1nR2VCnsBdIZ.wY3bd763UIH3tkQzUNwQ4sWVdLvsS9G', 'abudabi@asd.ru', null, '{"city": "", "code": "", "flat": "", "house": "", "porch": "", "street": "", "building": ""}', 'Цинис', '0', 'f', '0', 'f', 'f');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."banner_autoid" OWNED BY "banners"."banner_id";
ALTER SEQUENCE "public"."incexp_autoid" OWNED BY "incexps"."incexp_id";
ALTER SEQUENCE "public"."list_autoid" OWNED BY "lists"."list_id";
ALTER SEQUENCE "public"."order_autoid" OWNED BY "orders"."order_id";
ALTER SEQUENCE "public"."product_autoid" OWNED BY "products"."product_id";
ALTER SEQUENCE "public"."promo_autoid" OWNED BY "promos"."promo_id";
ALTER SEQUENCE "public"."user_autoid" OWNED BY "users"."user_id";

-- ----------------------------
-- Primary Key structure for table banners
-- ----------------------------
ALTER TABLE "public"."banners" ADD PRIMARY KEY ("banner_id");

-- ----------------------------
-- Primary Key structure for table colors
-- ----------------------------
ALTER TABLE "public"."colors" ADD PRIMARY KEY ("color_id");

-- ----------------------------
-- Primary Key structure for table incexps
-- ----------------------------
ALTER TABLE "public"."incexps" ADD PRIMARY KEY ("incexp_id");

-- ----------------------------
-- Primary Key structure for table lists
-- ----------------------------
ALTER TABLE "public"."lists" ADD PRIMARY KEY ("list_id");

-- ----------------------------
-- Triggers structure for table orders
-- ----------------------------
CREATE TRIGGER "NewSuccessOrder" AFTER UPDATE ON "public"."orders"
FOR EACH ROW
EXECUTE PROCEDURE "orders_count_recalc"();

-- ----------------------------
-- Primary Key structure for table orders
-- ----------------------------
ALTER TABLE "public"."orders" ADD PRIMARY KEY ("order_id");

-- ----------------------------
-- Primary Key structure for table positions
-- ----------------------------
ALTER TABLE "public"."positions" ADD PRIMARY KEY ("position");

-- ----------------------------
-- Indexes structure for table products
-- ----------------------------
CREATE UNIQUE INDEX "j_uuid_idx" ON "public"."products" USING btree (((reviews ->> 'user_id'::text)::integer));

-- ----------------------------
-- Triggers structure for table products
-- ----------------------------
CREATE TRIGGER "sizes_count_recalc" AFTER UPDATE OF "sizes" ON "public"."products"
FOR EACH ROW
EXECUTE PROCEDURE "sizes_count_recalc"();

-- ----------------------------
-- Uniques structure for table products
-- ----------------------------
ALTER TABLE "public"."products" ADD UNIQUE ("article_number");

-- ----------------------------
-- Primary Key structure for table products
-- ----------------------------
ALTER TABLE "public"."products" ADD PRIMARY KEY ("product_id");

-- ----------------------------
-- Indexes structure for table promos
-- ----------------------------
CREATE UNIQUE INDEX "name_index" ON "public"."promos" USING btree ("name");

-- ----------------------------
-- Primary Key structure for table promos
-- ----------------------------
ALTER TABLE "public"."promos" ADD PRIMARY KEY ("promo_id");

-- ----------------------------
-- Primary Key structure for table statuses
-- ----------------------------
ALTER TABLE "public"."statuses" ADD PRIMARY KEY ("status_id");

-- ----------------------------
-- Primary Key structure for table types
-- ----------------------------
ALTER TABLE "public"."types" ADD PRIMARY KEY ("type_id");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD PRIMARY KEY ("user_id");

-- ----------------------------
-- Foreign Key structure for table "public"."orders"
-- ----------------------------
ALTER TABLE "public"."orders" ADD FOREIGN KEY ("user_id") REFERENCES "public"."users" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."orders" ADD FOREIGN KEY ("status_id") REFERENCES "public"."statuses" ("status_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."orders" ADD FOREIGN KEY ("promo_id") REFERENCES "public"."promos" ("promo_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
