/*

Template:  Theme Name
Author: author name
Version: 1
Design and Developed by: BM Rafiq + Masud Rana
NOTE: If you have any note put here. 

*/
/*================================================
[  Table of contents  ]
================================================
	01. jQuery MeanMenu
	02. wow js active
	03. scrollUp jquery active
	04. slick carousel 

 
======================================
[ End table content ]
======================================*/


(function($) {
    "use strict";




/*----------------------------
  tooltiop
------------------------------ */ 
  
    $('[data-toggle="tooltip"]').tooltip({
        animated: 'fade',
        placement: 'top',
        container: 'body'
    }); 

/*------------------------------------------------
      Top menu stick
     -------------------------------------------------- */
$(window).scroll(function() {
if ($(this).scrollTop() > 20){  
    $('#sticky-header').addClass("sticky");
  }
  else{
    $('#sticky-header').removeClass("sticky");
  }

if ($(this).scrollTop() > 150){  
    $('#sticky-header2').addClass("sticky");
  }
  else{
    $('#sticky-header2').removeClass("sticky");
  }
});


    /*-------------------------------------------
    	01. jQuery MeanMenu
    --------------------------------------------- */
$('.mobile-menu nav').meanmenu({
	meanScreenWidth: "990",
	meanMenuContainer: ".mobile-menu",
});
/*-------------------------------------------
	02. wow js active
--------------------------------------------- */
    new WOW().init();


/*-------------------------------------------
   slider 1
--------------------------------------------- */  
      $('#nivoslider-style-1').nivoSlider({
        effect: 'random',
        slices: 15,
        boxCols: 8,
        boxRows: 4,
        animSpeed: 500,
        pauseTime: 5000,
        startSlide: 0,
        directionNav: false,
        controlNavThumbs: false,
        pauseOnHover: false,
        manualAdvance: true
       });  
  

/*-------------------------------------------
   slider 1
--------------------------------------------- */	
			$('#nivoslider-style-2').nivoSlider({
				effect: 'random',
				slices: 15,
				boxCols: 8,
				boxRows: 4,
				animSpeed: 500,
				pauseTime: 5000,
				startSlide: 0,
				directionNav: false,
				controlNavThumbs: true,
				pauseOnHover: false,
				manualAdvance: false
			 }); 	
	
	
    /*-------------------------------------------
    	03. scrollUp jquery active
    --------------------------------------------- */
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });

/*----------------------------
 product-carousel active 
------------------------------ */  
  $(".product-carousel").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:true,
      navigation:false,    
      items : 4,
      /* transitionStyle : "fade", */    /* [This code for animation ] */
      navigationText:["<span></span>","<span></span>"],
      itemsDesktop : [1199,4],
      itemsDesktopSmall : [980,3],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });
/*----------------------------
 product-carousel box active 
------------------------------ */  
  $(".product-carousel-box").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,    
      items : 4,
      navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,4],
      itemsDesktopSmall : [980,3],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });
/*----------------------------
 product-carousel-most-view active 
------------------------------ */  
  $(".product-carousel-most-view").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,    
      items : 3,
      navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });
/*----------------------------
  hot deals active 
------------------------------ */  
  $(".side-hot-deal-carousel").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,    
      items : 1,
      navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,2],
      itemsMobile : [479,1],
  });
/*----------------------------
  best seller carousel active 
------------------------------ */  
  $(".best-seller-carousel").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,    
      items : 1,
      navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,2],
      itemsMobile : [479,1],
  });
/*----------------------------
 testmonial-carousel active 
------------------------------ */    
  $("#testmonial-carousel").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:true,
      navigation:false,   
      items : 1,
      navigationText:["<span></span>","<span></span>"],
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,1],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });
/*----------------------------
 testmonial-carousel active 
------------------------------ */    
  $("#testmonial-carousel2").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,   
      items : 1,
      navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,1],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });

/*----------------------------
 blog-curosel active 
------------------------------ */    
  $("#blog-curosel").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:true,
      navigation:false,   
      items : 3,
      navigationText:["<span></span>","<span></span>"],
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });

/*----------------------------
 blog-curosel 4 active 
------------------------------ */    
  $("#blog-curosel-4").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,   
      items : 2,
      navigationText:["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,2],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });

/*----------------------------
 blog-curosel 5 active 
------------------------------ */    
  $("#blog-curosel-5").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,   
      items : 2,
      navigationText:["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,2],
      itemsDesktopSmall : [980,1],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });

/*----------------------------
blog-curosel-duble active 
------------------------------ */    
  $("#blog-curosel-duble").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,   
      items : 1,
      navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,1],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });
  $("#blog-curosel-6").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:true,   
      items : 3,
      navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });

/*----------------------------
 brand-carousel active 
------------------------------ */    
  $("#brand-curosel").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:false,
      navigation:false,   
      items : 6,
      navigationText:["<span></span>","<span></span>"],
      itemsDesktop : [1199,5],
      itemsDesktopSmall : [980,4],
      itemsTablet: [768,2],
      itemsMobile : [479,1],
  });
/*----------------------------
 slider-product-carousel active 
------------------------------ */    
  $(".slider-product-carousel").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:true,
      navigation:false,   
      items : 4,
      navigationText:["<span></span>","<span></span>"],
      itemsDesktop : [1199,4],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });
/*----------------------------
 bannner carousel active 
------------------------------ */    
  $(".banner-carousel").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:true,
      navigation:false,   
      items : 1,
      navigationText:["<span></span>","<span></span>"],
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,1],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });

/*----------------------------
single product carousel active 
------------------------------ */ 
	var sync1 = $("#sync1");
	var sync2 = $("#sync2");

	sync1.owlCarousel({
    items : 1,
    itemsDesktop: [1199, 1],
    itemsDesktopSmall: [980, 1],
    itemsTablet: [768, 1],
    itemsTabletSmall: false,
    itemsMobile: [479, 1],
		singleItem : true,
		slideSpeed : 1000,
		navigation: false,
		pagination:false,
		afterAction : syncPosition,
		responsiveRefreshRate : 200
	});

	sync2.owlCarousel({
		items : 3,
		itemsDesktop: [1199, 3],
		itemsDesktopSmall: [980, 3],
		itemsTablet: [768, 3],
		itemsTabletSmall: false,
		itemsMobile: [479, 2],
		pagination:false,
		responsiveRefreshRate : 100,
		navigation: true,
		navigationText: ["<span class='fa fa-angle-left'></span>", "<span class='fa fa-angle-right'></span>"],
		afterInit : function(el){
			el.find(".owl-item").eq(0).addClass("synced");
		}
	});

	function syncPosition(el){
		var current = this.currentItem;
		$("#sync2")
			.find(".owl-item")
			.removeClass("synced")
			.eq(current)
			.addClass("synced")
		if($("#sync2").data("owlCarousel") !== undefined){
			center(current)
		}
	}

	$("#sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	});

/*--------------------------
cart-plus-minus-button
---------------------------- */     
     $(".cart-plus-minus").append('<div class="dec qtybutton">-</div><div class="inc qtybutton">+</div>');
      $(".qtybutton").on("click", function() {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();
        if ($button.text() == "+") {
          var newVal = parseFloat(oldValue) + 1;
        } else {
           // Don't allow decrementing below zero
          if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
            } else {
            newVal = 0;
          }
          }
        $button.parent().find("input").val(newVal);
      });




$(".icon-search").on('click', function(){
  $(".top-form").show(500);
  $(".logo").hide();
});

$(".closebtn").on('click', function(){
  $(".top-form").hide(500);
  $(".logo").show();
});

/*---------------------
 price slider
--------------------- */  
    $( "#slider-range" ).slider({
     range: true,
     min: 40,
     max: 600,
     values: [ 60, 570 ],
     slide: function( event, ui ) {
    $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
     }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
     " - $" + $( "#slider-range" ).slider( "values", 1 ) );

/*----------------------------
	venobox
------------------------------ */	
	$('.venobox').venobox(); 
 /*---------------------
	Checkout page toggle
 --------------------- */  
  $(".showlogin").on('click', function(){
      $(".login").slideToggle();
    })
  $(".show-coupon").on('click', function(){
      $(".checkout_coupon").slideToggle();
    });
  $(".showaccount").on('click', function(){
      $(".account-box-hide").slideToggle();
    });
  $(".showship").on('click', function(){
      $(".ship-box-hide").slideToggle();
    });
  
 $(".payment_method-li").on('click', function(){
  $(".payment_method_bacs").show(500);
  $(".payment_method_cheque").hide(500);
  $(".payment_method_paypal").hide(500);
});
 $(".payment_method_cheque-li").on('click', function(){
  $(".payment_method_cheque").show(500);
  $(".payment_method_bacs").hide(500);
  $(".payment_method_paypal").hide(500);
});
 $(".payment_method_paypal-li").on('click', function(){
  $(".payment_method_paypal").show(500);
  $(".payment_method_cheque").hide(500);
  $(".payment_method_bacs").hide(500);
});


/*----------------------------
 blog-curosel active  shortcode
------------------------------ */    
  $("#blog-curosel-shortcode").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:true,
      navigation:false,   
      items : 3,
      navigationText:["<span></span>","<span></span>"],
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [980,2],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });

/*----------------------------
 testmonial-carousel active shortcode
------------------------------ */    
  $("#testmonial-carousel-shortcode").owlCarousel({
      autoPlay: false, 
      slideSpeed:2000,
      pagination:true,
      navigation:false,   
      items : 1,
      navigationText:["<span></span>","<span></span>"],
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,1],
      itemsTablet: [768,1],
      itemsMobile : [479,1],
  });







})(jQuery);
