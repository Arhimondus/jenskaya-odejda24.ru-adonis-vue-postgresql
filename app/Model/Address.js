'use strict'

const Lucid = use('Lucid')

class Address extends Lucid {
    static get rules() { 
        return {
            city: 'alpha_numeric',
            street: 'alpha_numeric',
            house: 'alpha_numeric',
            building: 'alpha_numeric',
            porch: 'numeric',
            flat: 'numeric',
            code: 'alpha_numeric',
        }
    }
}

module.exports = new Address()