'use strict'

const Lucid = use('Lucid')

class CheckoutOrder extends Lucid {
    static rules (userId, email) { 
        return {
            name: 'required|alpha',
            phone: 'required|phone',
            email: 'required|email',

            city: 'required|alpha_numeric',
            street: 'required|alpha_numeric',
            house: 'required|numeric',
            building: 'required|numeric',
            porch: 'required|alpha_numeric',
            flat: 'required|numeric',
            code: 'required|numeric',

            promo: 'required|alpha_numeric',
            use_bonuses: 'required|numeric',

            items: 'array|min:1',
            'items.*.product_id': 'required|numeric',
            'items.*.size': 'required|numeric',
            'items.*.computed_price': 'required|numeric',

            summary: 'required|numeric',
            delivery: 'required|numeric',
            total: 'required|numeric',

            comment: 'required|numeric'
        }
    }
}

module.exports = new CheckoutOrder()