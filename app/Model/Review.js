'use strict'

const Lucid = use('Lucid')
const validate = require('validate.js');
validate.options = { format: "flat" };

class Review extends Lucid {
    validate(query, review, user) { 
        let error = validate(review, {
  			name: {
				presence: true,
			},
			text: {
				presence: true
			},
			rate: {
				presence: true,
				numericality: {
					onlyInteger: true,
					greaterThanOrEqualTo: 1,
					lessThanOrEqualTo: 5
				}
            }
        }) || validate(query, {
            product_id: {
                presence: true,
				numericality: {
                    onlyInteger: true
                }
            }
        });
        if(error) return error;
    }
    prepare(review, user) {
        review.date = new Date().toISOString().substring(0, 10);
        review.user_id = user.user_id;
        return review;
    }
}

module.exports = new Review()