'use strict'

const Lucid = use('Lucid')
const validate = require('validate.js');
validate.options = { format: "flat" };
const bcrypt = require('bcryptjs');
const shortid = require('shortid');
const atob = require('atob');
const btoa = require('btoa');

class RescueFinal extends Lucid {
    prepare(request) {
		let rescue_data_b64 = request.input('rescue_data');
		let rescue_final = JSON.parse(atob(rescue_data_b64));
		let error = validate(rescue_final, {
  			user_id: {
				presence: true,
				numericality: {
					onlyInteger: true
				}
			},
			rescue_code: {
				presence: true
			}
        });
        if(error) return { error };
		return { rescue_final };
    }
}

module.exports = new RescueFinal()