'use strict'

const Lucid = use('Lucid')
const validate = require('validate.js');
validate.options = { format: "flat" };
const bcrypt = require('bcryptjs');
const shortid = require('shortid');
const atob = require('atob');
const btoa = require('btoa');

class Rescue extends Lucid {
    validate(rescue) { 
        let error = validate(rescue, {
  			email: {
				presence: true,
				email: true
			},
			password: {
				presence: true,
				length: { minimum: 6 }
			}
        });
        if(error) return error;
    }
    prepare(rescue) {
		let raw_password = rescue.password;
		let email = rescue.email.toLowerCase();
		console.log(rescue.email);
		console.log(email);
		rescue.rescue_password = bcrypt.hashSync(rescue.password, 10);
		delete rescue.password;
		rescue.rescue_code = shortid.generate();
		delete rescue.email;
        return { prepared: rescue, email, raw_password };
    }
	rescue_data({ rescue_code }, user_id) {
		return btoa(JSON.stringify({ user_id, rescue_code }));
	}
}

module.exports = new Rescue()