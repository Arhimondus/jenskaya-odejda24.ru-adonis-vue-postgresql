'use strict'

const Lucid = use('Lucid')
const validate = require('validate.js');
validate.options = { format: "flat" };

class Question extends Lucid {
    validate(query, question, user) { 
        let error = validate(question, {
  			name: {
				presence: true,
			},
			text: {
				presence: true
			}
        }) || validate(query, {
            product_id: {
                presence: true,
				numericality: {
                    onlyInteger: true
                }
            }
        });
        if(error) return error;
    }
    prepare(question, user) {
        question.date = new Date().toISOString().substring(0, 10);
        question.user_id = user.user_id;
        return question;
    }
}

module.exports = new Question()