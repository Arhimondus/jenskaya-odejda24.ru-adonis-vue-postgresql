'use strict'

const Lucid = use('Lucid')
const validate = require('validate.js');
validate.options = { format: "flat" };
const bcrypt = require('bcryptjs');
const shortid = require('shortid');
const atob = require('atob');
const btoa = require('btoa');

class Register extends Lucid {
    validate(register) { 
        let error = validate(register, {
  			email: {
				presence: true,
				email: true
			},
			password: {
				presence: true,
				length: {
					minimum: 6,
					tooShort: "^Необходимо ввести пароль не менее 6 символов",
				}
			},
			email_subscribe: {
				presence: true
			}
        });
        if(error) return error;
    }
    prepare(register) {
		register.password = bcrypt.hashSync(register.password, 10);
		register.activation_code = shortid.generate();
		register.email = register.email.toLowerCase();
        return register;
    }
	activation_data({ activation_code }, user_id) {
		return btoa(JSON.stringify({ user_id, activation_code }));
	}
}

module.exports = new Register()