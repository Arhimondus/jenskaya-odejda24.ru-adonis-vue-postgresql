'use strict'

const Lucid = use('Lucid')
const validate = require('validate.js');
validate.options = { format: "flat" };
const bcrypt = require('bcryptjs');
const shortid = require('shortid');
const atob = require('atob');
const btoa = require('btoa');

class ChangePassword extends Lucid {
    validate(change_password) { 
        let error = validate(change_password, {
  			old_password: {
				presence: true,
				length: { minimum: 6 }
			},
			new_password: {
				presence: true,
				length: { minimum: 6 }
			}
        });
        if(error) return error;
    }
    prepare(change_password) {
        return { old_password: change_password.old_password, new_password: change_password.new_password };
    }
}

module.exports = new ChangePassword()