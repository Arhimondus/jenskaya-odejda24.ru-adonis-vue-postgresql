'use strict'

const Lucid = use('Lucid')
const validate = require('validate.js');
validate.options = { format: "flat" };
const bcrypt = require('bcryptjs');
const shortid = require('shortid');
const atob = require('atob');
const btoa = require('btoa');

class Activate extends Lucid {
    prepare(request) {
		let activation_data_b64 = request.input('activation_data');
		let activate = JSON.parse(atob(activation_data_b64));
		let error = validate(activate, {
  			user_id: {
				presence: true,
				numericality: {
					onlyInteger: true
				}
			},
			activation_code: {
				presence: true
			}
        });
        if(error) return { error };
		return { activate };
    }
}

module.exports = new Activate()