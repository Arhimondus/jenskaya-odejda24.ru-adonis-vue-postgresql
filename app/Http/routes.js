'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/
const Database = use('Database')
const Route = use('Route')

Route.get('/api/category', 'ProductController.category')
Route.get('/api/category_filtered', 'ProductController.category_filtered')
Route.get('/api/subcategory', 'ProductController.subcategory')
Route.get('/api/novelty', 'ProductController.novelty')
Route.get('/api/main', 'ProductController.main')

Route.get('/api/product', 'ProductController.product')

Route.get('/api/products', 'ProductController.products')
Route.get('/api/list_main', 'ProductController.list_main')
Route.get('/api/list_bottom', 'ProductController.list_bottom')

Route.post('/api/login', 'UserController.login')
Route.post('/api/register', 'UserController.register')

Route.get('/api/activate', 'UserController.activate')
Route.get('/api/logout', 'UserController.logout')

Route.post('/api/rescue', 'UserController.rescue')
Route.get('/api/rescue_final', 'UserController.rescue_final')

Route.post('/api/change_password', 'UserController.change_password')

Route.get('/api/user_data', 'UserController.user_data')
Route.get('/api/user_orders', 'UserController.user_orders')

Route.post('/api/set_personal_data', 'UserController.set_personal_data')
Route.post('/api/set_address_data', 'UserController.set_address_data')


Route.post('/api/admin/image/upload', 'AdminController.imageUpload')
Route.get('/api/admin/product', 'AdminController.get_product')

Route.get('/api/admin/users', 'AdminController.users')

Route.get('/api/admin/banners', 'AdminController.banners')
Route.get('/api/admin/promos', 'AdminController.promos')
Route.get('/api/admin/orders', 'AdminController.orders')
Route.get('/api/admin/types', 'AdminController.types')
Route.get('/api/admin/positions', 'AdminController.positions')

Route.get('/api/admin/lists', 'AdminController.lists')
Route.post('/api/admin/list/save', 'AdminController.list_save')
Route.post('/api/admin/list/remove', 'AdminController.list_remove')

Route.post('/api/admin/product/save', 'AdminController.product_save')
Route.post('/api/admin/product/remove', 'AdminController.product_remove')
Route.post('/api/admin/product/income', 'AdminController.product_income')
Route.post('/api/admin/product/expense', 'AdminController.product_expense')

Route.post('/api/admin/promo/save', 'AdminController.promo_save')
Route.post('/api/admin/promo/remove', 'AdminController.promo_remove')

Route.post('/api/admin/order/change_status', 'AdminController.order_change_status');
Route.post('/api/admin/order/remove_item', 'AdminController.order_remove_item');
Route.post('/api/admin/order/add_item', 'AdminController.order_add_item');
Route.post('/api/admin/order/save', 'AdminController.order_save');

Route.post('/api/admin/banner/save', 'AdminController.banner_save')
Route.post('/api/admin/banner/remove', 'AdminController.banner_remove')

Route.get('/api/admin/order/get', 'AdminController.order_get');

Route.get('/api/admin/order/change_delivery_method', 'AdminController.change_delivery_method');

Route.get('/api/promo_check', 'MiscController.promo_check')
Route.get('/api/banners', 'MiscController.banners')

Route.get('/api/lists', 'MiscController.lists')
Route.get('/api/types', 'MiscController.types')
Route.get('/api/colors', 'MiscController.colors')
Route.get('/api/statuses', 'MiscController.statuses')
Route.get('/api/locations', 'MiscController.locations')
Route.get('/api/regions', 'MiscController.regions')

Route.post('/api/order', 'OrderController.order')

Route.post('/api/product/add_review', 'ProductController.add_review')
Route.post('/api/product/add_question', 'ProductController.add_question')

Route.post('/api/email_subscribe', 'UserController.email_subscribe')
Route.post('/api/email_unsubscribe', 'UserController.email_unsubscribe')
Route.post('/api/sms_subscribe', 'UserController.sms_subscribe')
Route.post('/api/sms_unsubscribe', 'UserController.sms_unsubscribe')

Route.post('/api/send_to_us', 'MailController.send_to_us')

Route.get('/api/products_all', 'ProductController.products_all')

Route.post('/api/add_to_cart', 'CartController.add_to_cart')
Route.post('/api/remove_from_cart', 'CartController.remove_from_cart')
Route.post('/api/empty_cart', 'CartController.empty_cart')
Route.post('/api/get_cart', 'CartController.get_cart')

if(process.env.NODE_ENV !== 'production') {
	Route.get('/debug/email/register', 'DebugController.register');
	Route.get('/debug/email/order', 'DebugController.order');
	Route.get('/debug/captcha', 'DebugController.captcha');
	Route.get('/debug/truncate/all', 'DebugController.truncate');
}

Route.any('*', 'NuxtController.render')