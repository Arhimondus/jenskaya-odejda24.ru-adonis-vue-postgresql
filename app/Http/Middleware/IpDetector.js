'use strict'

class IpDetector {
	* handle (request, response, next) {
		yield request.session.set('ip', request.ip());
		yield next;
	}
}

module.exports = IpDetector;