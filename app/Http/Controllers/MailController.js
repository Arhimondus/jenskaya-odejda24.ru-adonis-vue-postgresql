'use strict'

const Env = use('Env')
const Config = use('Config')
const Database = use('Database')
const transporter = require("bluebird").promisifyAll(require('nodemailer').createTransport(Config.get('mailSender')))
const validate = require('validate.js')

const CircularJSON = require('circular-json');
const winston = require('winston');
winston.add(winston.transports.File, { filename: 'mail-controller.log' });

class MailController {
	* send_to_us (request, response) {
		let user_req = request.post();
		let error = validate(user_req, {
  			name: {
				presence: {
					message: '^Введите пожалуйста ваше имя'
				}
			},
			topic: {
				presence: false
			},
			email: {
				presence: true,
				email: {
					message: '^Электронная почта не введена или неверная'
				}
			},
			message: {
				presence: {
					message: '^Сообщение должно быть введено'
				}
			}
        },{ format: "flat" });
		
		if(error)
			return response.status(400).json(error);
	
		try{
			if(process.env.NODE_ENV === 'production') {
				let email_to = Env.get('MAIL_USER');
				let result = yield transporter.sendMail({
					from: `Сообщение с сайта от ${user_req.name} <${email_to}>`,
					to: email_to,
					subject: user_req.topic,
					text: user_req.name + "\r\n" + user_req.email + "\r\n" + user_req.message,
				});
				response.send('ok');
			} else
				response.send('ok send');
		} catch(err) {
			winston.info(CircularJSON.stringify(err));
			throw err;
		}
	}
}

module.exports = new MailController()