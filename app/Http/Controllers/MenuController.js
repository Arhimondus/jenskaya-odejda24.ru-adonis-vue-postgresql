'use strict'

const Env = use('Env')
const Config = use('Config')
const Database = use('Database')

class MenuController {
	* list (request, response) {
		const lists = yield Database.select('*').from('lists').orderBy('order');
		
		response.json(lists);
	}
}

module.exports = new MenuController()