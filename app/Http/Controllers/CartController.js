'use strict'

const Env = use('Env')
const Config = use('Config')
const Helpers = use('Helpers')
const Database = use('Database')
const Validator = use('Validator')
const Redis = use('Redis')

class CartController {
	* add_to_cart (request, response) {
		let body = request.post();
		
		let product = body.product;
		let size = body.size;
		
		let cart = yield this.get_cart_helper(request, response);
		cart.items.push({ product, size });
		
		yield this.set_cart_helper(request, response, cart);
		
		response.send('ok');
	}
	* remove_from_cart (request, response) {
		let item = request.post();
		
		let cart = yield this.get_cart_helper(request, response);
		cart.items = cart.items.filter(f => f.product.product_id !== item.product.product_id && item.size == item.size);
		
		yield this.set_cart_helper(request, response, cart);
		
		response.send('ok');
	}
	* empty_cart (request, response) {
		let product_id = JSON.parse(request.input('product_id'));
		
		let cart = { items: [], is_loaded: false, total: 0, bonuses: 0, summary: 0, delivery: 0, delivery_method: 'courier', region: null };
		
		yield this.set_cart_helper(request, response, cart);
		
		response.send('ok');
	}
	* set_cart_helper (request, response, new_cart) {
		let user = yield request.session.get('user');
		let cart;
		if(user) {
			yield Redis.set('user_cart_' + user.user_id, JSON.stringify(new_cart));
		} else {
			yield Redis.set('guest_cart_' + request.session.sessionId, JSON.stringify(new_cart));
		}
		return cart;
	}
	* get_cart_helper (request, response) {
		let user = yield request.session.get('user');
		return yield this.get_cart_helper_from_user(user, request);
	}
	* get_cart_helper_from_user (user, request) {
		let cart;
		if(user) {
			try {
				cart = JSON.parse(yield Redis.get('user_cart_' + user.user_id));
				if(cart == null) throw new Error();
			} catch(err) {
				cart = { items: [], is_loaded: false, total: 0, bonuses: 0, summary: 0, delivery: 0, delivery_method: 'courier', region: null };
				yield Redis.set('user_cart_' + user.user_id, JSON.stringify(cart));
			}
		} else {
			try {
				cart = JSON.parse(yield Redis.get('guest_cart_' + request.session.sessionId));
				if(cart == null) throw new Error();
			} catch(err) {
				cart = { items: [], is_loaded: false, total: 0, bonuses: 0, summary: 0, delivery: 0, delivery_method: 'courier', region: null };
				yield Redis.set('guest_cart_' + request.session.sessionId, JSON.stringify(cart));
			}
		}
		return cart;
	}
	* get_cart (request, response) {
		let cart = yield this.get_cart_helper(request, response);
		response.json(cart);
	}
}

module.exports = new CartController()