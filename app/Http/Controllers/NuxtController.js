'use strict'

const Env = use('Env')
const Config = use('Config')
const { Nuxt, Builder } = require('nuxt')
const CartController = use('App/Http/Controllers/CartController');

class NuxtController {
	constructor () {
		let config = Config.get('nuxt')
		config.dev = Env.get('NODE_ENV') === 'development'
		this.nuxt = new Nuxt(config)
		if (config.dev) {
			let builder = new Builder(this.nuxt);
			builder.build();
		}
	}

	* render (request, response) {
		request.request.user = yield request.session.get('user');
		request.request.xsrfToken = request.csrfToken();
		request.request.baseUrl = Env.get('BASE_URL');
		request.request.cart = yield CartController.get_cart_helper_from_user(request.request.user, request);
		
		this.nuxt.render(request.request, response.response);
	}
}

module.exports = new NuxtController()