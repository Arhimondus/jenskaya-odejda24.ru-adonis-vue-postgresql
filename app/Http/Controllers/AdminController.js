'use strict'

const Env = use('Env')
const Config = use('Config')
const Database = use('Database')
const Helpers = use('Helpers')
// const Logger = use('Logger')

const shortid = require('shortid')
const pica = require('pica')();
const jimp = require('jimp');
const fs = require('fs');
const axios = require('axios');
const querystring = require('querystring');
const escape = require('pg-escape');
const computer = require('../../../resources/computer.js');

const UserController = use('App/Http/Controllers/UserController');

class AdminController {
	* product_remove (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let product_id = request.input('product_id');
		let result = yield Database.table('products').where('product_id', product_id).delete();
		response.json(result);
	}
	* product_save (request, response) {
		let time;
		
		//console.time('tt1');
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let images = JSON.parse(request.input('images'));
		let product_id = JSON.parse(request.input('product_id'));
		let name = JSON.parse(request.input('name'));
		let article_number = JSON.parse(request.input('article_number'));
		let description = JSON.parse(request.input('description'));

		let colors = JSON.parse(request.input('colors')) || null;
		let price = JSON.parse(request.input('price'));
		
		let base_price = JSON.parse(request.input('base_price'));
		let discount = JSON.parse(request.input('discount')) || 0;
		let tag = JSON.parse(request.input('tag'));

		let purchase_price = JSON.parse(request.input('purchase_price') || null);
		let pavilion = JSON.parse(request.input('pavilion') || null);
		let is_public = JSON.parse(request.input('public'));
		
		if(base_price > 30000) response.status(400).json(['Слишком большая цена!']);
			
		if(price == null) price = base_price;
		
		let type_id = JSON.parse(request.input('type_id'));
		
		//let sizes = request.input('sizes');
		let fields = request.input('fields');
		

		let count_free = JSON.parse(request.input('count_free'));

		let lists = request.input('lists');
	
		//console.timeEnd('tt1');
		
		//console.time('tt2');
		let raw_images = request.file('images_file');
		//if(!raw_images || raw_images.length < 2) return response.status(400).json(['Слишком мало изобржений!']);
		//console.timeEnd('tt2');
		
		//console.time('tt3');
		if(raw_images != null)
			for(let i=0;i<raw_images.length;i++) {
				let image = raw_images[i];
				if(!image) continue;
				let fileName = `${shortid.generate()}`;

				let source_for_full = yield jimp.read(image.file.path);
				source_for_full.quality(95).write(Helpers.publicPath() + "/pics/" + fileName + ".jpg");

				let source_for_thumbs = yield jimp.read(image.file.path);
				source_for_thumbs.cover(287, 410).quality(95).write(Helpers.publicPath() + "/pics/" + fileName + "_thumbs.jpg");

				/*jimp.read(image.file.path, function (err, source_for_full) {
					source_for_full.cover(770, 1000).quality(95).write(Helpers.publicPath() + "/pics/" + fileName + ".jpg");
				});
				
				jimp.read(image.file.path, function (err, source_for_thumbs) {
					source_for_thumbs.cover(261, 339).quality(95).write(Helpers.publicPath() + "/pics/" + fileName + "_thumbs.jpg");
				});*/
				
				images[i]=fileName;
			}
		images = images.filter(t => t);
		//console.timeEnd('tt3');
		
		//console.time('tt4');
		let type = yield Database.table('types').where('type_id', type_id).first();
		let sizes = type.sizes.map(f => ({ [f.size]: 0 })).reduce((a,b)=>Object.assign(a,b),{});
		//console.timeEnd('tt4');
		
		try {
			if(!product_id)
				yield Database.table('products').insert({
					name,
					article_number,
					type_id,
					lists,
					description,
					base_price,
					price,
					discount,
					tag,
					sizes,
					colors: JSON.stringify(colors),
					fields,
					count_free,
					purchase_price,
					pavilion,
					public: is_public,
					images: JSON.stringify(images),
				});
			else
				yield Database.table('products').where('product_id', product_id).update({
					name,
					article_number,
					type_id,
					lists,
					description,
					base_price,
					price,
					discount,
					tag,
					colors: JSON.stringify(colors),
					fields,
					count_free,
					purchase_price,
					pavilion,
					public: is_public,
					images: JSON.stringify(images),
				});
		} catch(err) {
			console.log(err);
			response.status(400).send(err);
		} finally {
			yield Database.raw(`UPDATE lists SET count=s.count
			FROM (
				SELECT q.value as list_id, COUNT(*) 
				FROM (
					SELECT products.product_id, products.name, lists.value::text::int
					FROM products, jsonb_array_elements(products.lists) as lists GROUP BY value, products.product_id, products.name
				) as q GROUP BY q.value
			) as s
			WHERE lists.list_id = s.list_id;`);
			let db_lists = yield Database.table('lists').select('list_id', 'max_size', 'count').whereIn('list_id', JSON.parse(lists)).where('max_size', '>', 0).whereRaw('count >= max_size');
			for(let list of db_lists)
				yield Database.raw(`UPDATE products as p
				SET lists = (SELECT array_to_json(array_agg(z.x)) FROM (SELECT jsonb_array_elements(l.lists) as x ) as z WHERE z.x <> '${list.list_id}')
				FROM (SELECT lists, product_id FROM products WHERE lists @> '${list.list_id}' ORDER BY product_id LIMIT ${list.count-list.max_size}) as l WHERE p.product_id = l.product_id;`);
		}
		response.send('ok');
	}
	* product_income (request, response) {
		let product_id = request.input('product_id');
		let income_sizes = request.post();
		
		let product = yield Database.table('products').where('product_id', product_id).first();
		
		const transaction = yield Database.beginTransaction();
		try {
			for(let size in income_sizes) {
				let count = income_sizes[size];
				if(count)
					yield transaction.raw(`UPDATE products SET sizes = jsonb_set(sizes, '{${size}}', (COALESCE(sizes->>'${size}','0')::int + ${count})::text::jsonb) WHERE product_id = ${product_id};`);
			}
			yield transaction.table('incexps').insert({ changes: income_sizes, product_id, name: product.name, type: 'income' });
			
			yield transaction.commit();
			response.send('ok?');
		} catch(err) {
			yield transaction.rollback();
			response.status(400).json(err);
		}
	}
	* product_expense (request, response) {
		let product_id = request.input('product_id');
		let expense_sizes = request.post();
		
		let product = yield Database.table('products').where('product_id', product_id).first();
		
		const transaction = yield Database.beginTransaction();
		try {
			for(let size in expense_sizes) {
				let count = expense_sizes[size];
				if(count)
					yield transaction.raw(`UPDATE products SET sizes = jsonb_set(sizes, '{${size}}', (COALESCE(sizes->>'${size}','0')::int - ${count})::text::jsonb) WHERE product_id = ${product_id};`);
			}
			yield transaction.table('incexps').insert({ changes: expense_sizes, product_id, name: product.name, type: 'expense' });
			
			yield transaction.commit();
			response.send('ok?');
		} catch(err) {
			yield transaction.rollback();
			response.status(400).json(err);
		}
	}
	* imageUpload (request, response) {
		let user = yield request.session.get('user');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		const imageFile = request.file('file', { 
			maxSize: '5mb',
			allowedExtensions: ['jpg']
		});
		yield imageFile.move(Helpers.publicPath(), shortid.generate() + ".jpg");
		if (!imageFile.moved()) {
			response.badRequest(imageFile.errors())
			return
		}
		response.ok(imageFile.uploadPath());
	}
	* orders (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let page = request.input('page');
		let order_id = request.input('order_id');
		
		let query = Database.table('orders').orderBy('order_id', 'desc');
		
		if(order_id)
			query.where('order_id', order_id);
		
		let result = yield query.paginate(page, 20);
		
		response.json(result);
	}
	* banners (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let result = yield Database.table('banners').select('*').orderBy('banner_id', 'desc');
		response.json(result);
	}
	* positions (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let result = yield Database.table('positions').select('*');
		response.json(result);
	}
	* users (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let result = yield Database.table('users').select('user_id', 'name', 'email').orderBy('user_id', 'desc');
		response.json(result);
	}
	* promos (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let result = yield Database.table('promos').select('*').orderBy('promo_id', 'desc');
		response.json(result);
	}
	* types (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let result = yield Database.table('types').select('*').orderBy('type_id');
		response.json(result);
	}
	* lists (request, response) {
		const page_size = 24;
		
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let list_name = request.input('list_name');
		let page = request.input('page', 1);
		
		let query = Database.table('lists').orderBy('name');
		
		if(list_name)
			query.whereRaw(escape('name LIKE %L', "%" + list_name + "%"));
		
		let result = yield query.paginate(page, page_size);
		
		response.json(result);
	}
	* questions (request, response) {
		/*let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let result = yield Database.table('products').select('*').orderBy('promo_id', 'desc');
		response.json(result);*/
	}
	* list_save (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let list = request.post();
		if(!list.list_id)
			delete list.list_id;
		
		list.articles = JSON.parse(list.articles) || null;
		if(list.articles != null) list.articles = JSON.stringify(list.articles);
		list.parent_id = JSON.parse(list.parent_id) || null;
		list.type_id = JSON.parse(list.type_id) || null;
		list.in_menu = JSON.parse(list.in_menu);
		list.in_main = JSON.parse(list.in_main);
		list.in_catalog = JSON.parse(list.in_catalog);
		list.column = JSON.parse(list.column) || null;
		list.max_size = JSON.parse(list.max_size) || null;
		list.meta_description = JSON.parse(list.meta_description);
		list.meta_keywords = JSON.parse(list.meta_keywords);
		list.catalog_title = JSON.parse(list.catalog_title);
		list.uri = JSON.parse(list.uri);
		list.name = JSON.parse(list.name);
		list.title = JSON.parse(list.title);
		list.color = JSON.parse(list.color);
		list.order = JSON.parse(list.order);
		list.header_bg = JSON.parse(list.header_bg);
		
		let image = request.file('header_bg_file');
		if(image) {
			let fileName = `${shortid.generate()}`;
			list.header_bg = fileName;
			
			let source_image = yield jimp.read(image.file.path);
			source_image.cover(1920, 130).quality(95).write(Helpers.publicPath() + "/pics/" + fileName + ".jpg");
		}
		delete list.header_bg_file;
		if(!list.list_id)
			yield Database.table('lists').insert(list);
		else 
			yield Database.table('lists').where('list_id',list.list_id).update(list);
		response.send('ok');
	}
	* banner_save (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let banner = request.post();
		if(!banner.banner_id)
			delete banner.banner_id;
		
		banner.name = JSON.parse(banner.name) || null;
		banner.position = JSON.parse(banner.position) || null;
		banner.alt = JSON.parse(banner.alt) || null;
		banner.order = JSON.parse(banner.order) || null;
		banner.uri = JSON.parse(banner.uri) || null;
		banner.image = JSON.parse(banner.image) || null;
		banner.order = JSON.parse(banner.order) || 0;
		
		let image = request.file('image_file');
		if(image) {
			let fileName = `${shortid.generate()}`;
			banner.image = fileName;
			
			let source_image = yield jimp.read(image.file.path);
			source_image.quality(95).write(Helpers.publicPath() + "/pics/" + fileName + ".jpg");
		}
		delete banner.image_file;
		if(!banner.banner_id)
			yield Database.table('banners').insert(banner);
		else 
			yield Database.table('banners').where('banner_id', banner.banner_id).update(banner);
		response.send('ok');
	}
	* banner_remove (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let banner_id = request.input('banner_id');
		let result = yield Database.table('banners').where('banner_id', banner_id).delete();
		response.json(result);
	}
	* list_remove (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let list_id = request.input('list_id');
		let result = yield Database.table('lists').where('list_id', list_id).delete();
		response.json(result);
	}
	* get_product(request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');

		let product_id = request.input('product_id');
		let result = yield Database.table('products').where('product_id', product_id).first();
		response.json(result);
	}
	* order_get(request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');

		let order_id = request.input('order_id');
		let result = yield Database.table('orders').where('order_id', order_id).first();
		response.json(result);
	}
	* promo_save (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let promo = request.post();
		promo.last_date = promo.last_date || null;
		
		if(!promo.promo_id)
			yield Database.table('promos').insert(promo);
		else 
			yield Database.table('promos').where('promo_id', promo.promo_id).update(promo);
		response.send('ok');
	}
	* promo_remove (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let promo_id = request.input('promo_id');
		let result = yield Database.table('promos').where('promo_id', promo_id).delete();
		response.json(result);
	}
	* order_change_status (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
	
		let order_id = request.input('order_id')-0;
		let status_id = request.input('status_id')-0;
		
		if(status_id == 3) {
			const transaction = yield Database.beginTransaction();
			try {
				let $order = yield transaction.table('orders').select('items', 'bonuses').where('order_id', order_id).whereNot('status_id', 3).first();
				if(!$order)
					throw ['Заказ с подходящим статусом не найден'];
				//Возврат товара
				for(let item of $order.items) {
					yield transaction.raw(`UPDATE products SET sizes = jsonb_set(sizes, '{${item.size}}', (COALESCE(sizes->>'${item.size}','0')::int + 1)::text::jsonb) WHERE product_id = ${item.product_id};`);
				}
				//Возврат бонусов
				if($order.bonuses)
					yield transaction.table('users').where('user_id', user.user_id).increment('bonuses', $order.bonuses);
				yield transaction.table('orders').where('order_id', order_id).update({ status_id });
				yield transaction.commit();
			} catch(err) {
				yield transaction.rollback();
				return response.status(400).json(err);
			}
		} else if(status_id == 4) {
			const transaction = yield Database.beginTransaction();
			let $order = null;
			try {
				$order = yield transaction.table('orders').select('user_id', 'order_id', 'total').where('order_id', order_id).where('status_id', 2).first();
				if(!$order)
					throw ['Заказ с подходящим статусом не найден'];	
				let accrued_bonuses = Math.ceil($order.total * 0.05);
				yield transaction.table('users').increment('bonuses', accrued_bonuses).where('user_id', $order.user_id);
				yield transaction.table('orders').update({ accrued: accrued_bonuses, status_id }).where('order_id', $order.order_id);
				yield transaction.commit();
			} catch(err) {
				yield transaction.rollback();
				return response.status(400).json(err);
			}
			
			let $user = yield Database.table('users').where('user_id', $order.user_id).first();
			if($user.email_subscribe)
				yield UserController.unisender_email_subscribe($user.name, $user.email, $user.orders, request.ip());
		} else
			yield Database.table('orders').where('order_id', order_id).update({ status_id });
			
		response.send('ok');
	}
	* order_remove_item(request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
	
		let order_id = request.input('order_id')-0;
		let item_id = request.input('item_id')-0;
		
		const transaction = yield Database.beginTransaction();
		try {
			let order = yield transaction.table('orders').where('order_id', order_id).whereNot('status_id', 3).whereNot('status_id', 4).first();
			let item_index = order.items.findIndex(f => f.id == item_id);
			let order_item = order.items[item_index];
			let returning_items = (yield transaction.table('orders').where('order_id', order_id).decrement('items', item_index).returning('items'))[0];
			yield transaction.raw(`UPDATE products SET sizes = jsonb_set(sizes, '{${order_item.size}}', (COALESCE(sizes->>'${order_item.size}','0')::int + 1)::text::jsonb) WHERE product_id = ${order_item.product_id};`);
			let status_id = order.status_id;
			if(returning_items.length == 0)
				status_id = (yield transaction.table('orders').where('order_id', order_id).update('status_id', 3).returning('status_id'))[0];
			
			let summary =  order.summary - order_item.computed_price;
			let total = summary + order.delivery - order.bonuses;

			yield transaction.table('orders').where('order_id', order_id).update({ summary, total });
			//yield transaction.table('orders').where('order_id', order_id).update({ summary, delivery, total });
			
			yield transaction.commit();
			
			// Logger.info(`Удаление товара ${order_id} из заказа ${item_id} успешно выполнено (AdminController/order_remove_item)`)
			
			response.json({ items: returning_items, summary, total, status_id });
		} catch(err) {
			yield transaction.rollback();
			// Logger.error(`Удаление товара ${order_id} из заказа ${item_id} (AdminController/order_remove_item)`)
			return response.status(400).json(err);
		}
	}
	* order_add_item(request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
	
		let order_id = request.input('order_id')-0;
			
		let { article_number, size, computed_price } = request.post();
		
		const transaction = yield Database.beginTransaction();
		try {
			let order = yield transaction.table('orders').where('order_id', order_id).whereNot('status_id', 3).whereNot('status_id', 4).first();
			
			let product = yield transaction.table('products').where('article_number', article_number).where('count', '>', 0).first();
			computed_price = (computed_price || product.price)-0;
			
			let new_item = {
				id: order.items[order.items.length - 1].id,
				name: product.name,
				size: size-0,
				product_id: product.product_id,
				article_number: article_number,
				computed_price: computed_price,
			};
			yield transaction.raw('UPDATE orders SET items = items || ? WHERE order_id = ?', [JSON.stringify(new_item), order_id]);
					
			let result = yield transaction.raw(`UPDATE products SET sizes = jsonb_set(sizes, '{${size}}', (COALESCE(sizes->>'${size}','0')::int - 1)::text::jsonb) WHERE product_id = ${product.product_id} RETURNING sizes;`);
			if(result.rows[0].sizes[new_item.size] < 0)
				throw ["Товара нет в наличии!"];
			
			let summary =  order.summary + new_item.computed_price;
			let total = summary + order.delivery - order.bonuses;
			
			yield transaction.table('orders').where('order_id', order_id).update({ summary, total });
			//yield transaction.table('orders').where('order_id', order_id).update({ summary, delivery, total });
			
			yield transaction.commit();
			response.json({ new_item, summary, total });
		} catch(err) { 
			yield transaction.rollback();
			return response.status(400).json(err);
		}
	}
	* change_delivery_method (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let order_id = request.input('order_id')-0;
		let new_delivery_method = request.input('new_delivery_method');
		
		let $order = yield Database.table('orders').where('order_id', order_id).first();
		
		let $region_data = (yield Database.table('regions').where('region_id', $order.region_id).first())['delivery_' + new_delivery_method];
		
		let delivery = computer.delivery($order.summary, $order.bonuses, $region_data)
		let total = $order.summary + delivery - $order.bonuses;
		
		yield Database.table('orders').where('order_id', order_id).update({
			delivery,
			total,
			delivery_method: new_delivery_method,
		});
			
		response.json({ delivery, total });
	}
	* order_save (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		if(!user.is_admin) return response.status(403).send('Forbidden');
		
		let order = request.post();
		
		let $order = yield Database.table('orders').where('order_id', order.order_id).first();
		if($order.delivery != order.delivery) {
			order.total = $order.summary + (order.delivery-0) - $order.bonuses;
		}
		yield Database.table('orders').where('order_id', order.order_id).update(order);
			
		response.send('ok');
	}
}

module.exports = new AdminController()