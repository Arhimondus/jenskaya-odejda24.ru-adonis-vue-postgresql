'use strict'

const Env = use('Env')
const Config = use('Config')
const Helpers = use('Helpers')

class DebugController {
	* register(request, response) {
		let email = request.input('email');
		response.download(Helpers.publicPath(`debug/email_register_${email}.html`));
	}
	* order(request, response) {
		let email = request.input('email');
		response.download(Helpers.publicPath(`debug/email_order_${email}.html`));
	}
	* captcha(request, response) {
		response.send('secret_captcha');
	}
	* truncate(request, response) {
		yield Database.raw('TRUNCATE TABLE product RESTART IDENTITY;');
		yield Database.raw('TRUNCATE TABLE order RESTART IDENTITY;');
		yield Database.raw('TRUNCATE TABLE user RESTART IDENTITY;');
		yield Database.raw('TRUNCATE TABLE list RESTART IDENTITY;');
		yield Database.raw('TRUNCATE TABLE banner RESTART IDENTITY;');
		yield Database.raw('TRUNCATE TABLE promo RESTART IDENTITY;');
		yield Database.raw('TRUNCATE TABLE category RESTART IDENTITY;');
	}
}

module.exports = new DebugController()