'use strict'

const Env = use('Env')
const Config = use('Config')
const Helpers = use('Helpers')
const Database = use('Database')
const Validator = use('Validator')
const AddressModel = use('App/Model/Address')
const RegisterModel = use('App/Model/Register')
const RescueModel = use('App/Model/Rescue')
const RescueFinalModel = use('App/Model/RescueFinal')
const ActivateModel = use('App/Model/Activate')
const ChangePasswordModel = use('App/Model/ChangePassword')

const bcrypt = require('bcryptjs');
const fs = require('fs');

const Handlebars = require('handlebars');
const registerTemplate = Handlebars.compile(fs.readFileSync("mail_templates/register.html", "utf-8")); 
const rescueTemplate = Handlebars.compile(fs.readFileSync("mail_templates/rescue.html", "utf-8")); 
const changeTemplate = Handlebars.compile(fs.readFileSync("mail_templates/change.html", "utf-8")); 
const transporter = require("bluebird").promisifyAll(require('nodemailer').createTransport(Config.get('mailSender')))

const validate = require('validate.js');
const axios = require('axios');
const CircularJSON = require('circular-json');
const querystring = require('querystring');
const winston = require('winston');
winston.add(winston.transports.File, { filename: 'user-controller.log' });

class UserController {
	* register (request, response) {
		try {
			let register = request.post();
		
			let error = RegisterModel.validate(register);
			if(error) return response.status(400).json(error);
			
			let raw_password = register.password;
			
			let prepared = RegisterModel.prepare(register);
			let user_id = (yield Database.table('users').insert(prepared).returning('user_id'))[0];
			
			let htmlData = registerTemplate({ 
				activation_url: Env.get('BASE_URL') + "/api/activate/?activation_data=" + RegisterModel.activation_data(prepared, user_id),
				email: prepared.email.replace('@', '<img/>@<img/>'),
				password: raw_password
			});		
				
			if(process.env.NODE_ENV === 'production') {
				let email_from = Env.get('MAIL_USER');
				let result = yield transporter.sendMail({
					from: `"Интернет-магазин Для Милых Дам ✅" <${email_from}>`,
					to: `${prepared.email}`,
					subject: `Регистрация в личном кабинете ✔`,
					text: 'Откройте html версию для просмотра письма.',
					html: htmlData
				});
				response.send('ok');
			} else {
				fs.writeFileSync(Helpers.publicPath(`debug/email_register_${prepared.email}.html`), htmlData);
				response.json('ok');
			}
		} catch(err) {
			if(err.code == 23505)
				return response.status(400).json(["Такая учётная запись уже существует!"]);
			winston.info(CircularJSON.stringify(err));
			throw err;
		}
	}
	* login (request, response) {
		let email = request.input('email').toLowerCase();
		let password = request.input('password');
		let result = yield Database.table('users').select('user_id', 'password', 'is_admin', 'name', 'email').where('email', email).whereNull('activation_code').first();
		
		if(!result || !bcrypt.compareSync(password, result.password))
			return response.status(400).json(['Неверно введена почта или пароль']);
	
		let user = { 
			is_admin: result.is_admin,
			user_id: result.user_id,
			name: result.name,
			email: result.email,
		};

		yield request.session.put('user', user);
		
		response.json(user);
	}
	* logout (request, response) {
		yield request.session.forget('user');
		response.send('ok');
	}
	* rescue (request, response) {
		try {
			let rescue = request.post();
		
			let error = RescueModel.validate(rescue);
			if(error) return response.status(400).json(error);
			
			let { prepared, email, raw_password } = RescueModel.prepare(rescue);
			
			let user_id = (yield Database.table('users').update(prepared).where('email', email).returning('user_id'))[0];
			
			if(!user_id) throw new Error('Такой почты у нас нету! Проверьте ввод.');
			
			let htmlData = rescueTemplate({ 
				rescue_url: Env.get('BASE_URL') + "/api/rescue_final/?rescue_data=" + RescueModel.rescue_data(prepared, user_id),
				new_password: raw_password,
				email: email.replace('@', '<img/>@<img/>')
			});		
		
			if(process.env.NODE_ENV === 'production') {
				let email_from = Env.get('MAIL_USER');
				let result = yield transporter.sendMail({
					from: `"Интернет-магазин Для Милых Дам 🔑" <${email_from}>`,
					to: `${email}`,
					subject: `Восстановление забытого пароля ✔`,
					text: 'Откройте html версию для просмотра письма.',
					html: htmlData
				});
				response.send('ok');
			} else {
				fs.writeFileSync(Helpers.publicPath(`debug/email_rescue_${email}.html`), htmlData);
				response.json('ok');
			}
		} catch(err) {
			winston.info(CircularJSON.stringify(err));
			response.status(400).json([err.message]);
		}
	}
	* rescue_final (request, response) {
		let { error, rescue_final } = RescueFinalModel.prepare(request);
		if(error) return response.status(400).json(error);
		
		let result = yield Database.table('users').select('user_id', 'name', 'rescue_password', 'email').where('user_id', rescue_final.user_id).whereNotNull('rescue_code').where('rescue_code', rescue_final.rescue_code).first();
		if(result) { 
			yield Database.table('users').update({ rescue_code: null, rescue_password: null, password: result.rescue_password }).where('user_id', result.user_id);
			let user = { 
				user_id: result.user_id,
				name: result.name,
				email: result.email,
			};
			yield request.session.put('user', user);
			response.redirect('/myaccount');
		} else
			response.redirect('/');
	}
	* change_password (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		
		try {
			let change_password = request.post();
		
			let error = ChangePasswordModel.validate(change_password);
			if(error) return response.status(400).json(error);
			
			let { new_password, old_password } = ChangePasswordModel.prepare(change_password);
			
			let $user = yield Database.table('users').where('user_id', user.user_id).first();
		
			if(!bcrypt.compareSync(change_password.old_password, $user.password)) {
				return response.status(400).json(['Вы ввели неверный текущий пароль!']);
			}
			
			let user_id = (yield Database.table('users').update('password', bcrypt.hashSync(change_password.new_password, 10)).where('user_id', $user.user_id).returning('user_id'))[0];
			
			let htmlData = changeTemplate({ 
				new_password: change_password.new_password,
				email: $user.email.replace('@', '<img/>@<img/>')
			});		
			
			if(process.env.NODE_ENV === 'production') {
				let email_from = Env.get('MAIL_USER');
				let result = yield transporter.sendMail({
					from: `"Интернет-магазин Для Милых Дам 🔑" <${email_from}>`,
					to: `${$user.email}`,
					subject: `Смена пароля ✔`,
					text: 'Откройте html версию для просмотра письма.',
					html: htmlData
				});
				response.send('ok');
			} else {
				fs.writeFileSync(Helpers.publicPath(`debug/email_change_${$user.email}.html`), htmlData);
				response.json('ok');
			}
		} catch(err) {
			winston.info(CircularJSON.stringify(err));
			throw err;
		}
	}
	* activate (request, response) {
		let { error, activate } = ActivateModel.prepare(request);
		if(error) return response.status(400).json(error);
		
		let result = yield Database.table('users').select('user_id', 'name', 'email_subscribe', 'email', 'orders').where('user_id', activate.user_id).where('activation_code', activate.activation_code).first();
		if(result) { 
			yield Database.table('users').update('activation_code', null).where('user_id', result.user_id);
			if(result.email_subscribe)
				yield this.unisender_email_subscribe(result.name || ('Клиент №' + result.user_id), result.email, result.orders, request.ip());
			let user = { 
				user_id: result.user_id,
				name: result.name,
				email: result.email,
			};
			yield request.session.put('user', user);
			response.redirect('/myaccount');
		} else
			response.redirect('/');
	}
	* user_data (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');

		let result = yield Database.table('users').select('address', 'phone', 'name', 'email', 'bonuses', 'sms_subscribe', 'email_subscribe').where('user_id', user.user_id).first();
		response.json(result);
	}
	* user_orders (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');

		let result = yield Database.table('orders').select('*').where('user_id', user.user_id).orderBy('datetime', 'desc').limit(5);
		response.json(result);
	}
	* set_personal_data (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		
		let name = request.input('name');
		let phone = request.input('phone');
		
		let userFromDb = yield Database.table('users').where('user_id', user.user_id).first();

		yield Database.table('users').update({
			name, phone
		}).where('user_id', user.user_id);

		if(userFromDb.phone != phone) {
			yield this.unisender_sms_unsubscribe(userFromDb.phone);
			yield this.unisender_sms_subscribe(userFromDb.name, userFromDb.orders, phone);
		}
		
		response.send('ok');
	}
	* set_address_data (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');

		const addressData = request.only('city', 'street', 'house', 'building', 'porch', 'flat', 'code');
		//const addressData = request.only('address');

		response.json(addressData);
    	const validation = yield Validator.validate(addressData, AddressModel.rules); 

		if (validation.fails())
			return response.json(validation.messages()) 
		
		yield Database.table('users').where('user_id', user.user_id).update({
			address: JSON.stringify(addressData)
		});

		response.send('ok');
	}
	* unisender_email_subscribe(name, email, orders, ip) {
		let e = yield axios.post('https://api.unisender.com/ru/api/subscribe', querystring.stringify({
			api_key: '63y3foqj3j9mj7nsob5zfti177mta4qm4kz8pa8a',
			list_ids: 10627397,
			double_optin: 3,
			overwrite: 1,
			request_ip: ip,
			'fields[Name]': name,
			'fields[Orders]': orders,
			'fields[email]': email
		}));
		winston.info(`unisender_email_subscribe ${email} status [${e.status}]`);
	}
	* unisender_email_unsubscribe(email) {
		let e = yield axios.post('https://api.unisender.com/ru/api/exclude', querystring.stringify({
			api_key: '63y3foqj3j9mj7nsob5zfti177mta4qm4kz8pa8a',
			contact_type: 'email',
			contact: email,
			list_ids: 10627397
		}));
		winston.info(`unisender_email_unsubscribe ${email} status [${e.status}]`);
	}
	* email_subscribe (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		let userFromDb = yield Database.table('users').where('user_id', user.user_id).first();
		
		yield this.unisender_email_subscribe(userFromDb.name, userFromDb.email, userFromDb.orders, request.ip());
		
		yield Database.table('users').update('email_subscribe', true).where('user_id', user.user_id);
		response.send('ok');
	}
	* email_unsubscribe (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		let userFromDb = yield Database.table('users').where('user_id', user.user_id).first();
		
		yield this.unisender_email_unsubscribe(userFromDb.email);
		
		yield Database.table('users').update('email_subscribe', false).where('user_id', user.user_id);
		response.send('ok');
	}
	* unisender_sms_subscribe (name, orders, phone) {
		let e = yield axios.post('https://api.unisender.com/ru/api/subscribe', querystring.stringify({
			api_key: '63y3foqj3j9mj7nsob5zfti177mta4qm4kz8pa8a',
			double_optin: 3,
			overwrite: 1,
			list_ids: 10627397,
			'fields[Name]': name,
			'fields[Orders]': orders,
			'fields[phone]': phone
		}));
		winston.info(`unisender_sms_subscribe ${email} status [${e.status}]`);
	}
	* unisender_sms_unsubscribe (phone) {
		let e = yield axios.post('https://api.unisender.com/ru/api/exclude', querystring.stringify({
			api_key: '63y3foqj3j9mj7nsob5zfti177mta4qm4kz8pa8a',
			contact_type: 'phone',
			contact: phone,
			list_ids: 10627397
		}));
		winston.info(CircularJSON.stringify(e));
	}
	* sms_subscribe (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		let userFromDb = yield Database.table('users').where('user_id', user.user_id).first();
		if(!userFromDb.phone) return response.status(400).json(['Вам необходимо сперва указать свой телефон персональных данных!']);
		
		yield this.unisender_sms_subscribe(userFromDb.name, userFromDb.orders, userFromDb.phone);
		
		yield Database.table('users').update('sms_subscribe', true).where('user_id', user.user_id);
		response.send('ok');
	}
	* sms_unsubscribe (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		let userFromDb = yield Database.table('users').where('user_id', user.user_id).first();
		if(!userFromDb.phone) return response.status(400).json(['Вам необходимо сперва указать свой телефон в персональных данных!']);
		
		yield this.unisender_sms_unsubscribe(userFromDb.phone);
		
		yield Database.table('users').update('sms_subscribe', false).where('user_id', user.user_id);
		response.send('ok');
	}
	* subscribe_guest (request, response) {
		let email = request.input('email');
		let phone = request.input('phone');

		let postObj = {
			api_key: '63y3foqj3j9mj7nsob5zfti177mta4qm4kz8pa8a',
			double_optin: 3,
			overwrite: 1,
			list_ids: 10627397
		}
		
		if(email)
			postObj['fields[email]'] = email;
		if(phone)
			postObj['fields[phone]'] = phone;
		
		yield axios.post('https://api.unisender.com/ru/api/subscribe?format=json',  {
			api_key: '63y3foqj3j9mj7nsob5zfti177mta4qm4kz8pa8a',
			double_optin: 0	
		});

		response.send('ok');
	}
}

module.exports = new UserController()