'use strict'

const Env = use('Env')
const Config = use('Config')
const Database = use('Database')
const Helpers = use('Helpers')
const CheckoutOrder = use('App/Model/CheckoutOrder')
const MiscController = use('App/Http/Controllers/MiscController')
const UserController = use('App/Http/Controllers/UserController')

const CircularJSON = require('circular-json');

const Handlebars = require('handlebars');
require('handlebars-helpers')({ handlebars: Handlebars });

const computer = require('../../../resources/computer.js');
const transporter = require("bluebird").promisifyAll(require('nodemailer').createTransport(Config.get('mailSender')));
const fs = require('fs');

const orderTemplate = Handlebars.compile(fs.readFileSync("mail_templates/order.html", "utf-8")); 

const shortid = require('shortid');
const bcrypt = require('bcryptjs');

const registerTemplate = Handlebars.compile(fs.readFileSync("mail_templates/register.html", "utf-8")); 

const validate = require("validate.js");

const winston = require('winston');
//winston.add(winston.transports.File, { filename: 'order-controller2.log' });

class OrderController {
    * order (request, response) {
		let cart = request.post(); //{ items, summary, delivery, total, bonuses, promo, promo_value, address, personal 
		let email_subscribe = JSON.parse(request.input('email_subscribe'));
		
		let user = yield request.session.get('user');
		if(user) {
			cart.user_id = user.user_id;
			cart.personal.email = (yield Database.table('users').select('email').where('user_id', cart.user_id).first()).email;
		} else {
			cart.user_id = null;
			cart.personal.email = cart.personal.email.toLowerCase();
		}
		
		let error = validate(cart.personal, {
			email: { 
				email: { message: "^Неверная электронная почта!" },
				presence: { message: "^Не введена электронная почта!" }
			},
			phone: {
				presence: { message: "^Необходимо ввести действительный номер телефона!" }
			}
		});
		
		if(error) return response.status(400).json(error);
		
		if(cart.promo) {
			let $promo = yield MiscController.promo_check_raw(cart.promo.name, user);
			if($promo) {
				if(cart.promo.discount != $promo.discount)
					return response.status(400).json(["Возможно устаревший промокод! Перепримените его."]);
				cart.promo.promo_id = $promo.promo_id;
			} else {
				return response.status(400).json(["Некорректно применённый промокод!"]);
			}
		}
		
		//validate.js!!! или сразу после let cart = ...
		
		let truelyCart = null;
		let order_id = null;
		
		const transaction = yield Database.beginTransaction();
		try {	
			let region = yield transaction.table('regions').where('region_id', cart.region_id).select('*').first();
			if(!region)
				throw ["Неопознанный регион"];
			if(!region[`delivery_${cart.delivery_method}`])
				throw ["Неподдерживаемый способ доставки"];
				
			if(cart.total < region[`delivery_${cart.delivery_method}`].minimal_price) 
				throw ["Не набрана минимальная сумма товаров"];
			
			let wrongProducts = [];
			let truelyItems = [];
			for(let i = 0; i < cart.items.length; i++) {
				let product_id = cart.items[i].product_id;
				let product = yield transaction.table('products').select('name', 'article_number', 'price', 'type_id', 'base_price', 'sizes', 'discount', 'count_free', 'images').where('product_id', product_id).first();
				truelyItems.push({ product_id, product, size: cart.items[i].size });
				if(!product) wrongProducts.push({ error: 'not_found_with_this_id', item_index: i });
			}
			if(wrongProducts.length > 0)
				throw { sp_errors: wrongProducts };
			truelyCart = computer.recompute({ 
				items: truelyItems,
				bonuses: cart.bonuses,
				promo: cart.promo,
				delivery_method: cart.delivery_method,
				region: region,
			});
			
			for(let i = 0; i < cart.items.length; i++) {
				let item = cart.items[i];
				let truelyItem = truelyCart.items[i];
				if(item.computed_price != truelyItem.computed_price)
					wrongProducts.push({ error: 'product_wrong_price', item_index: i, request_price: item.computed_price, real_price: truelyItem.computed_price, truelyCart });
				
				let type = yield transaction.table('types').select('sizes').where('type_id', truelyItem.product.type_id).first();
				let findedSize = type.sizes.find(f => f.size == item.size);
				if(!findedSize)
					wrongProducts.push({ error: 'product_undefined_size', item_index: i });
				
				let result = yield transaction.raw(`UPDATE products SET sizes = jsonb_set(sizes, \'{${item.size}}\', (COALESCE(sizes->>\'${item.size}\',\'0\')::int - 1)::text::jsonb) WHERE product_id = ${item.product_id-0} RETURNING sizes;`);
				if(result.rows[0].sizes[item.size] < 0)
					wrongProducts.push({ error: 'product_not_anymore', item_index: i });
			}
			if(wrongProducts.length > 0)
				throw { sp_errors: wrongProducts };
			
			if(cart.summary != truelyCart.summary)
				throw ["Некорректно вычисленная сумма товаров"];

			if(cart.bonuses > Math.floor(cart.summary * 0.5))
				throw ["Попытка использовать слишком много бонусов"];
			
			if(cart.delivery != truelyCart.delivery)
				throw ["Некорректно вычисленная сумма доставки", truelyCart.delivery];

			if(cart.total != truelyCart.total)
				throw ["Некорректно вычисленная итоговая сумма"];
			
			for(var i = 0; i < cart.items.length; i++) {
				cart.items[i].name = truelyCart.items[i].product.name;
				cart.items[i].article_number = truelyCart.items[i].product.article_number;
				cart.items[i].id = i + 1;
			}
			
			if(cart.user_id) {
				let resultBonuses = (yield transaction.table('users').decrement('bonuses', cart.bonuses).where('user_id', cart.user_id).returning('bonuses'))[0];
				if(resultBonuses < 0)
					throw ["Недостаточно бонусов"];
			}
						
			let address = cart.delivery_method != 'pvz' ? { 
				city: cart.address.city,
				street: cart.address.street,
				house: cart.address.house,
				building: cart.address.building,
				porch: cart.address.porch,
				flat: cart.address.flat,
				code: cart.address.code,
			} : {
				title: cart.address.title,
				address: cart.address.address,
			};
			address.region = region.name;
			
			order_id = yield transaction.table('orders').insert({ 
				user_id: cart.user_id, 
				items: JSON.stringify(cart.items),
				personal: { 
					name: cart.personal.name,
					phone: cart.personal.phone,
					email: cart.personal.email
				},
				address,
				comment: cart.comment,
				summary: cart.summary,
				delivery: cart.delivery,
				total: cart.total,
				bonuses: cart.bonuses,
				promo: cart.promo,
				delivery_method: cart.delivery_method,
				region_id: region.region_id,
			}).returning('order_id');
			
			yield transaction.commit();
		}
		catch(err) {
			yield transaction.rollback();
			winston.error(err);
			return response.status(400).json(err);
		}
		
		if(cart.user_id == null)
			cart.user_id = yield this.autoregister(cart, order_id[0]);
		else
			yield Database.table('users').update({ address: cart.address, name: cart.personal.name, phone: cart.personal.phone }).where('user_id', cart.user_id);
		
		yield this.sendorderinfo(cart, truelyCart, order_id[0]);
		
		if(email_subscribe) {
			try {
				let $user = yield Database.table('users').where('user_id', cart.user_id).first();
				if(!$user.email_subscribe) {
					yield UserController.unisender_email_subscribe($user.name, $user.email, $user.orders, request.ip());
					yield Database.table('users').where('user_id', cart.user_id).update('email_subscribe', true);
				}
			} catch(err) {
				winston.error(err);
			}
		}
		
		response.send('ok');
	}
	* autoregister ({ personal, address }, assigned_order_id) {
		try {
			let $already_user = yield Database.table('users').where('email', personal.email).first();
			if($already_user && assigned_order_id) {
				let x = yield Database.table('orders').update({ user_id: $already_user.user_id }).where('order_id', assigned_order_id);
				return $already_user.user_id;
			} else {
				let name = personal.name;
				let phone = personal.phone;
				let password = shortid.generate();
				
				let new_user_id = yield Database.table('users').insert({
					password: bcrypt.hashSync(password, 10),
					activation_code: null,
					email: personal.email.toLowerCase(),
					phone: personal.phone,
					name: personal.name,
					address,
				}).returning('user_id');
						
				yield Database.table('orders').update({ user_id: new_user_id[0] }).where('order_id', assigned_order_id);
				let registerHtmlData = registerTemplate({
					password: password,
					email: personal.email
				});
				
				if(process.env.NODE_ENV === 'production') {
					let email_from = Env.get('MAIL_USER');
					yield transporter.sendMail({
						from: `"Интернет-магазин Для Милых Дам ✅" <${email_from}>`,
						to: `${personal.email}`,
						subject: `Регистрация в личном кабинете ✔`,
						text: 'Откройте html версию для просмотра письма.',
						html: registerHtmlData
					});
					winston.info('Новый автопользователь ' + personal.email);
				} else {
					fs.writeFileSync(Helpers.publicPath(`debug/email_register_${personal.email}.html`), registerHtmlData);
				}
				return new_user_id[0];
			}
		} catch(err) {
			winston.error(err);
			return false;
		}
	}
	* sendorderinfo (cart, truelyCart, order_id) {
		try {
			let address = '';
			switch(cart.delivery_method) {
				case 'courier':
					address = `${cart.address.city}, улица ${cart.address.street}, ${cart.address.house} ${cart.address.building}, подъезд ${cart.address.porch}, код ${cart.address.code}, квартира/офис ${cart.address.flat}`;
					break;
				case 'pvz':
					address = `${cart.address.title}, ${cart.address.address}`;
					break;
				case 'post':
					address = `${cart.address.code} ${cart.address.city}, улица ${cart.address.street}, ${cart.address.house} ${cart.address.building}, подъезд ${cart.address.porch}, квартира/офис ${cart.address.flat}`;
					break;
			}
			let orderHtmlData = orderTemplate({ 
				cart: truelyCart,
				address,
				personal: cart.personal.name + ', ' + cart.personal.phone,
				order_id
			});
			
			if(process.env.NODE_ENV === 'production') {
				let email_from = Env.get('MAIL_USER');
				yield transporter.sendMail({
					from: `"Интернет-магазин Для Милых Дам 🛒" <${email_from}>`,
					to: `${cart.personal.email}`,
					bcc: email_from,
					subject: `Спасибо за покупку. Ваш заказ принят! ✔`,
					text: 'Откройте html версию для просмотра письма.',
					html: orderHtmlData
				});
				winston.info(`Новый заказ №{order_id} ${cart.personal.email}`);
				return true;
			} else {
				fs.writeFileSync(Helpers.publicPath(`debug/email_order_${cart.personal.email}.html`), orderHtmlData);
				return true;
			}
		} catch(err) {
			winston.error(err);
			return false;
		}
	}
}

module.exports = new OrderController()