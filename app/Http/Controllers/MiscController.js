'use strict'

const Env = use('Env')
const Config = use('Config')
const Database = use('Database')
const computer = require('../../../resources/computer.js');

class MiscController {
	* promo_check (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');
		
		let promo = request.input('promo');

		let $promo = yield this.promo_check_raw(promo, user);
		
		if($promo) response.json($promo.discount);
		else response.status(404).send('Promocode Not Found');
	}
	* promo_check_raw(promo_name, user) {
		let now = new Date();
		
		const $promo = yield Database.select('*').from('promos').where('name', promo_name).where('enabled', 1).first();

		let is_okay = $promo ? true : false;

		if(is_okay && $promo.last_date && new Date($promo.last_date) < now)
			is_okay = false;

		if(is_okay && $promo.single_use) {
			let orders = yield Database.table('orders').whereRaw(`user_id = :user_id AND promo->'promo_id' = :promo_id::jsonb`, { user_id: user.user_id, promo_id: $promo.promo_id });
			if(orders && orders.length > 0)
				is_okay = false;
		}
		return is_okay ? $promo : null;
	}
	* banners (request, response) {
		//let positions = request.input('position').split(',');
		//let result = yield Database.table('banners').select('*').whereIn('position', positions);
		let position = request.input('position');
		let result = yield Database.table('banners').select('*').where('position', position).orderBy('order');
		response.json(result);
	}
	* types (request, response) {
		let result = yield Database.table('types').select('*').orderBy('type_id');
		response.json(result);
	}
	* colors (request, response) {
		let result = yield Database.table('colors').select('*');
		response.json(result);
	}
	* lists (request, response) {
		let lists = yield Database.select('*').from('lists').orderBy('order');
		response.json(lists);
	}
	* statuses (request, response) {
		let result = yield Database.table('statuses').select('*');
		response.json(result);
	}
	* locations (request, response) {
		let region = request.input('region');
		let result = yield Database.table('locations').select('*').where('region', region);
		response.json(result);
	}
	* regions (request, response) {
		let result = yield Database.table('regions').select('*').orderBy('name');
		response.json(result);
	}
}

module.exports = new MiscController()