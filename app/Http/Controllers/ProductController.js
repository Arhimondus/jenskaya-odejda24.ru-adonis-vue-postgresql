'use strict'

const Env = use('Env')
const Config = use('Config')
const Database = use('Database')
const ReviewModel = use('App/Model/Review');
const QuestionModel = use('App/Model/Question');
const escape = require('pg-escape');

class ProductController {
	* category (request, response) {
		const page_size = 108;

		let category = request.input('category');

		let color = request.input('color');
		let page = request.input('page', 1);
		let size = request.input('size');
		let price = request.input('price');
		let subcategory = request.input('subcategory');

		let query = Database.table('products').where('category_id', category);

		if(color)
			query.where('color_id', color);
		if(size)
			query.whereRaw('sizes \\? ?', size);		
		if(subcategory)
			query.whereRaw('subcategories @> ?', subcategory);
		if(price) {
			let [minprice,maxprice] = price.split(',');
			query.whereBetween('price', [minprice, maxprice]);
		}

		const result = yield query.paginate(page, page_size);
		
		response.json(result);
	}
	* products (request, response) {
		const page_size = 108;

		let list = JSON.parse(request.input('list'));

		let article_number = request.input('article_number');
		let color = request.input('color');
		let page = request.input('page', 1);
		let size = request.input('size');
		let price = request.input('price');
		let sections = request.input('sections');

		let query = Database.table('products').where('public', true).where('count', '>', 0).orderBy('product_id', 'desc');

		if(article_number)
			query.whereRaw(escape('article_number LIKE %L', '%' + article_number + '%'));
		if(list)
			query.whereRaw('lists @> ?', list);
		if(color)
			query.whereRaw('colors @> ?', color);
		if(size)
			query.whereRaw('(sizes::jsonb->>?)::integer > 0', size);		
		if(sections)
			query.whereRaw('lists->>sections @> ?', sections);
		if(price) {
			let [minprice,maxprice] = price.split(',');
			query.whereBetween('price', [minprice, maxprice]);
		}

		const result = yield query.paginate(page, page_size);
		
		response.json(result);
	}
	* products_all (request, response) {
		const page_size = 108;

		let list = JSON.parse(request.input('list'));

		let article_number = request.input('article_number');
		let color = request.input('color');
		let page = request.input('page', 1);
		let size = request.input('size');
		let price = request.input('price');
		let sections = request.input('sections');

		let query = Database.table('products').orderBy('product_id', 'desc');

		if(article_number)
			query.whereRaw(escape('article_number LIKE %L', '%' + article_number + '%'));
		if(list)
			query.whereRaw('lists @> ?', list);
		if(color)
			query.whereRaw('colors @> ?', color);
		if(size)
			query.whereRaw('(sizes::jsonb->>?)::integer > 0', size);		
		if(sections)
			query.whereRaw('lists->>sections @> ?', sections);
		if(price) {
			let [minprice,maxprice] = price.split(',');
			query.whereBetween('price', [minprice, maxprice]);
		}

		const result = yield query.paginate(page, page_size);
		
		response.json(result);
	}
	* list_main (request, response) {
		let list = yield Database.table('lists').where('in_main', true).first();
		let numbered = yield Database.table('products').where('public', true).where('count', '>', 0).whereRaw('lists @> ?', list.list_id).paginate(1, 25);
		
		response.json({ list, numbered });
	}
	* list_bottom (request, response) {
		let list = yield Database.table('lists').where('on_bottom', true).first();
		let numbered = yield Database.table('products').where('public', true).where('count', '>', 0).whereRaw('lists @> ?', list.list_id).paginate(1, 4);
		
		response.json({ list, numbered });
	}
	* product (request, response) {
		let product_id = request.input('product_id');
		let result = yield Database.table('products').where('product_id', product_id).first();
		if(result)
			response.json(result);
		else
			response.status(404).send('Not Found');
	}
	* add_review (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');

		let query = request.get();
		let review = request.post();

		let error = ReviewModel.validate(query, review, user);
		if(error) return response.status(400).json(error);
		
		yield Database.raw('UPDATE products SET reviews = reviews || ? WHERE product_id = ?', [JSON.stringify(ReviewModel.prepare(review, user)), query.product_id]);

		response.send('ok');
	}
	* add_question (request, response) {
		let user = yield request.session.get('user');
		if(!user) return response.status(401).send('Unauthorized');

		let query = request.get();
		let question = request.post();
		
		let error = QuestionModel.validate(query, question, user);
		if(error) return response.status(400).json(error);
		
		yield Database.raw('UPDATE products SET questions = questions || ? WHERE product_id = ?', [JSON.stringify(QuestionModel.prepare(question, user)), query.product_id]);

		response.send('ok');
	}
}

module.exports = new ProductController()