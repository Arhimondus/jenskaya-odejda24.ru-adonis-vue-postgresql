'use strict'

const Env = use('Env')
const Helpers = use('Helpers')

module.exports = {
	/*
	|--------------------------------------------------------------------------
	| Default Connection
	|--------------------------------------------------------------------------
	|
	| Connection defines the default connection settings to be used while
	| interacting with SQL databases.
	|
	*/
	connection: 'pg',
	/*
	|--------------------------------------------------------------------------
	| PostgreSQL
	|--------------------------------------------------------------------------
	|
	| Here we define connection settings for PostgreSQL database.
	|
	| npm i --save pg
	|
	*/
	pg: {
	client: 'pg',
		connection: {
			host: Env.get('DB_HOST', 'localhost'),
			user: Env.get('DB_USER', 'root'),
			password: Env.get('DB_PASSWORD', ''),
			database: Env.get('DB_DATABASE', 'shop')
		}
	}
}