'use strict'

const resolve = require('path').resolve
const Env = use('Env')

module.exports = {
	/*
	** Headers of the page
	*/
	head: {
		title: 'Для милых дам!',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' }
			//{ hid: 'description', name: 'description', content: 'Для милых дам!' }
		],
		script: [
			{ src: 'https://unpkg.com/core-js@2.5.1/client/core' },
			// { src: 'https://www.googletagmanager.com/gtag/js?id=UA-120250872-1', async: true }
		]
	},
	attribute: '',
	/*
	** Global CSS
	*/
	/*css: [
	'/css/bootstrap.min.css',
	'/css/core.css',
	'/css/shortcode/shortcodes.css',
	'/venobox/venobox.css',
	'/css/custom.css',
	'/css/bozon.css',
	'/css/responsive.css'
	],
	script: [
	'/js/vendor/jquery-1.12.4.min.js',
	'/js/bootstrap.min.js',
	'/js/wow.min.js',
	'/js/owl.carousel.min.js',
	'/js/jquery.nivo.slider.pack.js',
	'/js/jquery-ui.min.js',
	'/js/jquery.meanmenu.js',
	'/venobox/venobox.min.js',
	'/js/jquery.waypoints.min.js',
	'/js/jquery.scrollUp.js',
	'/js/plugins.js',
	'/js/main.js'
	],*/
	/*
	** Customize the progress-bar color
	*/
	loading: { color: '#744d82' },
	/*
	** Point to resources
	*/
	srcDir: resolve(__dirname, '..', 'resources'),
	build: {
		vendor: ['axios']
	},
	router: {
		middleware: ['menuCategories'],
		//mode: 'history',
		/*routes: [
			{ path: '/:category', component: 'pages/_category' },
			//{ path: '/:category/:product_id(\\d+)/*', component: 'pages/_product' },
			{ path: '/:category/:subcategory', component: 'pages/_subcategory' }
		],*/
		/*extendRoutes (routes) {
			routes.push({ path: '/:category', component: resolve(__dirname, '..', 'resources') + '/pages/_category.vue' });
			//routes.push({ path: '/:category/:product_id(\\d+)/*', component: resolve(__dirname, '..', 'resources') + '/pages/_product.vue' });
			routes.push({ path: '/:category/:subcategory', component: resolve(__dirname, '..', 'resources') + '/pages/_subcategory.vue' });
		}*/
		/*extendRoutes (routes) {
			routes.push({ path: '/:category', component: '~/pages_c/_category.vue' });
		}*/
		scrollBehavior (to, from, savedPosition) {
			if(savedPosition) {
				return savedPosition
			} else {
				return { x: 0, y: 0 }
			}
		},
	},
	plugins: [
		'~/plugins/api.js',
		'~/plugins/helper.js',
		{ src: '~/plugins/notify.js', ssr: false },
		{ src: '~/plugins/vue-social-sharing.js', ssr: true },
		{ src: '~/plugins/ymetrika.js', ssr: false },
		{ src: '~/plugins/mytarget.js', ssr: false },
		{ src: '~/plugins/retargeting.js', ssr: false },
		{ src: '~/plugins/ganalytics.js', ssr: false },
	],
	env: {
		baseUrl: Env.get('BASE_URL')
	}
}