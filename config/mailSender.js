'use strict'

const Env = use('Env')
const Helpers = use('Helpers')

module.exports = {
	host: Env.get('MAIL_HOST', 'smtp.yandex.ru'),
	//service: Env.get('MAIL_SERVICE', 'Yandex'),
	secure: true,
	auth: {
		user: Env.get('MAIL_USER', 'user@yandex.ru'),
        pass: Env.get('MAIL_PASSWORD', 'password')
	}
}