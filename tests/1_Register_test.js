
Feature('Register');

Scenario('Регистрация нового пользователя', (I) => {
	I.amOnPage('/');
	I.click('Вход и регистрация');
	I.see('Ещё не зарегистрировались?');
	I.click('Зарегистрироваться');
		I.see('НОВЫЙ ПОЛЬЗОВАТЕЛЬ');
		I.fillField('email', 'robot@gmail.com');
		I.fillField('password', 'goreprogrammist666');
		I.fillField('password2', 'goreprogrammist666');
		I.click('.form-box button'); //Регистрация
		I.see('Для завершения регистрации зайдите в свою почту и перейдите по ссылке подтверждения.');
		I.dontSee('Новый пользователь');
		
	I.amOnPage('/debug/email/register?email=robot@gmail.com');
	I.see('Регистрация');
	I.click('Подтвердить регистрацию');
	
	I.seeInCurrentUrl('/myaccount');
	I.see('ЛИЧНЫЙ КАБИНЕТ');
});