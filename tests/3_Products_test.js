
Feature('Products', {timeout: 5000});

Scenario('Просмотр списка товаров и заказ', (I) => {
	I.amOnPage('/');
	I.click('Вход и регистрация');
	I.see('ВХОД В ЛИЧНЫЙ КАБИНЕТ');
	I.fillField('email', 'robot@test-email-9013.com');
	I.fillField('password', 'goreprogrammist666');
	I.click('Войти');
	I.see('Вы успешно вошли в личный кабинет');
	I.see('Личный кабинет');
	
	I.amOnPage('/');
	I.click('Каталог');
	I.wait('.mega-menu-area');
	I.click('Джинсы');
	I.see('Цвет');
	I.see('Размер');
	I.see('Цена');
	I.click('Подробно');
	I.click('Выберите размер');
	I.click('32 / 34 RUS');
	I.click('ДОБАВИТЬ В КОРЗИНУ');
	
	I.amOnPage('checkout');
	I.click('Оформить');
	
	I.amOnPage('/debug/email/order?email=robot@test-email-9013.com');
	I.see('Ваш заказ собирается');
});
