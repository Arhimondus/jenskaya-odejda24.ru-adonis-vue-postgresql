
'use strict';
// in this file you can append custom step methods to 'I' object

module.exports = function() {
	return actor({

		// Define custom steps here, use 'this' to access default methods of I.
		// It is recommended to place a general 'login' function here.
		fillInput: function(label, content) {
			this.fillField(`//*[contains(text(), '${label}')]/following::input[1]`, content);
		},
		fillInputP: function(parent, label, content) {
			this.fillField(`//*[contains(text(), ${parent})]/*[contains(text(), '${label}')]/following::input[1]`, content);
		},
		fillTextarea: function(label, content) {
			this.fillField(`//*[contains(text(), '${label}')]/following::textarea[1]`, content);
		},
		fillSelect: function(label, content) {
			this.selectOption(`//*[contains(text(), '${label}')]/following::select[1]`, content);
		}
	});
}
