
Feature('Login');

Scenario('Вход зарегистрированного пользователя', (I) => {
	I.amOnPage('/');
	I.click('Вход и регистрация');
	I.see('ВХОД В ЛИЧНЫЙ КАБИНЕТ');
	I.fillField('email', 'robot@test-email-9013.com');
	I.fillField('password', 'goreprogrammist666');
	I.click('Войти');
	I.see('Вы успешно вошли в личный кабинет');
	I.see('Личный кабинет');
});
