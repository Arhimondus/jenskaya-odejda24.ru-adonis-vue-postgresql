const Computer = {
	compute_total(product, promo, cart = null) {
		let price = product.price;
		if(!promo)
			return price;
		else
			return Math.floor(price * (100 - promo.discount) / 100);
	},
	recompute(cart) {
		if(!cart.region) return cart;
			
		for(let j = 0; j < cart.items.length; j++) {
			let item2 = cart.items[j];
			item2.computed_price = this.compute_total(item2.product, cart.promo);
		}
		for(var x = 2; x <= 4; x++) {
			let count_free_products = cart.items.filter(f => f.product.count_free == x);
			count_free_products.forEach((item, index) => {
				if((index + 1) % x == 0)
					item.computed_price = 0;
			});
		}
		
		let { value, free_price, minimal_price } = cart.region[`delivery_${cart.delivery_method}`];
		cart.summary = cart.items.reduce((a,b) => a+b.computed_price, 0);
		cart.delivery = ((cart.summary - cart.bonuses) < free_price || free_price == -1) ? value : 0;
		cart.total = cart.summary - cart.bonuses + cart.delivery;
		return cart;
	},
	delivery(summary, bonuses, { value, free_price, minimal_price }) {
		return (summary - bonuses) < free_price ? value : 0;
	},
}

module.exports = Computer;