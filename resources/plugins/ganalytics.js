export default ({ app, store }) => {
	// if (process.env.NODE_ENV !== 'production') return
	$.getScript("https://www.googletagmanager.com/gtag/js?id=UA-120250872-1", function() {
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-120250872-1', {
			debug: true,
			user_id: store.state.user ? store.state.user.user_id : 0
		});
		// gtag('set', { 'user_id': store.state.user ? store.state.user.user_id : 0 });
	});
}