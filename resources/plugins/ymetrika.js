/* eslint-disable */

export default ({ app, route }) => {
	if(process.env.NODE_ENV !== 'production') return;
	
	(function (d, w, c) {
		//if(w.yaCounter46476306) return;
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter46476306 = new Ya.Metrika({
                    id:46476306,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    //trackHash:true,
                    //ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
}