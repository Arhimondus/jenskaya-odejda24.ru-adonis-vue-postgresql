import Vue from 'vue'
import computer from '../computer.js'

let helper = {
	image(image) {
		if(image == null) return null;
		return '/pics/' + image + '.jpg';
	},
	thumbs(image) {
		if(image == null) return null;
		return '/pics/' + image + '_thumbs.jpg';
	},
	compute_total(item, cart, promo_value) {
		return computer.compute_total(item.product, promo_value || null, cart);
	},
	count(sizes) {
		return Object.entries(sizes).reduce((a,b)=>a+b[1],0);
	},
	product_url(product) {
		return '/p/' + product.product_id;
	},
	recompute(cart) {
		computer.recompute(cart.items);
	},
	to_absolute_url(store, url) {
		return store.state.baseUrl + url;
	}
};

Vue.use({
	install(Vue, options) {
		Vue.prototype.$helper = helper;
	}
});

export default ({ app, store }) => app.helper = helper;