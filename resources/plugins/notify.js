import Vue from 'vue'

Vue.use({
	install(Vue, options) {
		Vue.prototype.$notify = {
			error: (err) => { 
				let message;
				if(typeof(err) == 'string')
					message = err;
				else if(err.response && Array.isArray(err.response.data) && err.response.status == 400)
					message = err.response.data.reduce((a,b)=>a+"<br/>"+b);
				else if(err)
					message = err.toString();
				else
					message = "Произошла системная ошибка, попробуйте позднее или сообщите об этом нам.";

				$.notify({ icon: 'ti-na', message }, { type: 'danger', timer: 1000, delay: 2000, z_index: 3000, placement: { from: "top", align: "center" } });
			},
			success: (message) => $.notify({ icon: 'ti-check', message }, { type: 'success', timer: 1000, delay: 2000, z_index: 3000, placement: { from: "top", align: "center" }  })
		};
	}
});
