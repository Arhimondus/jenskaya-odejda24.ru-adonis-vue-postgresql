import Vue from 'vue'
import axios from 'axios'

let api = (req, isServer) => {
	let options = {};
	if (req && isServer) {
		options.baseURL = `http://${process.env.HOST || 'localhost'}:${process.env.PORT || 3000}/api/`
		options.headers = req.headers;
	} else
		options.baseURL = '/api/';
	return axios.create(options);
};

Vue.use({
	install(Vue, options) {
		Vue.prototype.$api = api();
	}
});

export default ({ req, isServer, app }) => app.api = api(req, isServer);