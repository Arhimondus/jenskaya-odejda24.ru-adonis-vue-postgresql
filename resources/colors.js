const Colors = [
	{ class: "black", name: "Чёрный", id: 1 },
	{ class: "white", name: "Белый", id: 2 },
	{ class: "red", name: "Красный", id: 3 },
	{ class: "blue", name: "Синий", id: 4 }
]

module.exports = Colors