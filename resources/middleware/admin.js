import axios from 'axios'

export default async function ({ isServer, store, error, redirect, req, res }) {
	if(store.state.user == null || !store.state.user.is_admin)
		return error({ statusCode: 403, message: 'Доступ запрещён!' });
}