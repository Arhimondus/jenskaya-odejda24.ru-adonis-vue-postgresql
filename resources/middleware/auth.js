import axios from 'axios'

export default async function ({ isServer, store, req, route, error, redirect }) {
	if(store.state.user == null || !store.state.user.is_admin)
		return redirect('/');
}