import axios from 'axios'

export default async function ({ isServer, store, req, app }) {
	if(isServer) {
		let lists = (await app.api.get('lists')).data;
		let types = (await app.api.get('types')).data;
		let colors = (await app.api.get('colors')).data;
		let statuses = (await app.api.get('statuses')).data;
		let list_bottom = (await app.api.get('list_bottom')).data;
			
		store.commit('SET_LISTS', lists);
		store.commit('SET_TYPES', types);
		store.commit('SET_COLORS', colors);
		store.commit('SET_STATUSES', statuses);
		store.commit('SET_LIST_BOTTOM', list_bottom);
	}
}