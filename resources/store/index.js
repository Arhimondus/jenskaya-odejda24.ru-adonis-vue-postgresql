import Vue from 'vue'
import Vuex from 'vuex'
const computer = require('../computer.js');

Vue.use(Vuex);

let store = () => new Vuex.Store({
	state: {
		xsrfToken: '',
		pageTitle: 'Default Title',
		categories: [],
		lists: [],
		types: [],
		colors: [],
		user: null,
		cart: { items: [], is_loaded: false, total: 0, bonuses: 0, summary: 0, delivery: 0, delivery_method: null, region: null },
		wishlist: [],
		recently: [],
		currentProduct: null,
		currentList: null,
		htmlModal: null,
		filter: null,
		products: null,
		statuses: [],
		baseUrl: null,
		list_bottom: { list: null, numbered: { data: [] } },
	},
	mutations: {
		//SHOW_MODAL(state, modal)
		SET_HTML_MODAL (state, html) {
			state.htmlModal = html;
		},
		SET_PAGE_TITLE (state, title) {
			state.pageTitle = title;
		},
		SET_CATEGORIES (state, categories) {
			state.categories = categories;
		},
		SET_LISTS (state, lists) {
			state.lists = lists;
		},
		SET_LIST_BOTTOM (state, list_bottom) {
			state.list_bottom = list_bottom;
		},
		SET_TYPES (state, types) {
			state.types = types;
		},
		SET_COLORS (state, colors) {
			state.colors = colors;
		},
		SET_USER (state, user) {
			state.user = user;
		},
		SET_CART_PROMO(state, promo) {
			state.cart.promo = promo;
			computer.recompute(state.cart);
		},
		RESET_CART_PROMO(state) {
			state.cart.promo = null;
			computer.recompute(state.cart);
		},
		SET_CART(state, items) {
			//debugger;
			state.cart.items = items.filter(f=>f);
			computer.recompute(state.cart);
			//state.cart.is_loaded = true;
		},
		EMPTY_CART(state) {
			state.cart.items = [];
			window.store.set('cart', state.cart.items);
		},
		ADD_TO_CART(state, { product, size, vue }) {
			state.cart.items.push({ product, size });
			computer.recompute(state.cart);
			
			vue.$api.post('add_to_cart', { product, size });
			
			window.store.set('cart', state.cart.items);
		},
		REMOVE_FROM_CART(state, { item, vue }) {
			state.cart.items = state.cart.items.filter(f => f !== item);
			computer.recompute(state.cart);

			vue.$api.post('remove_from_cart', item);
			
			window.store.set('cart', state.cart.items);
		},
		SET_CART_BONUSES(state, bonuses) {
			state.cart.bonuses = bonuses;
			computer.recompute(state.cart);
		},
		REMOVE_FROM_CART_BY_INDEX(state, item_index) {
			state.cart.items.splice(item_index, 1);
			computer.recompute(state.cart);
			window.store.set('cart', state.cart.items);
		},
		SET_WISHLIST(state, wishlist) {
			state.wishlist = wishlist.filter(f=>f);
			//state.wishlist.is_loaded = true;
		},
		CLEAR_WISHLIST(state) {
			state.wishlist = [];
			window.store.set('wishlist', state.wishlist);
		},
		ADD_TO_WISHLIST(state, product) {
			state.wishlist.push(product);

			window.store.set('wishlist', state.wishlist);
		},
		REMOVE_FROM_WISHLIST(state, product) {
			state.wishlist = state.wishlist.filter(f => f != product);
		
			window.store.set('wishlist', state.wishlist);
		},
		SET_RECENTLY(state, recently) {
			state.recently = recently.filter(f=>f);
		},
		ADD_TO_RECENTLY(state, product) {
			if(state.recently == null) return;
			if(state.recently.findIndex(f=>f.product_id == product.product_id) < 0)
				state.recently.push(product);
			if(state.recently.length > 6)
				state.recently.shift();
			window.store.set('recently', state.recently);
		},
		SET_CURRENT_PRODUCT(state, product) {
			state.currentProduct = product;
		},
		SET_FILTER(state, filter) {
			state.filter = filter;
		},
		SET_PRODUCTS(state, products) {
			state.products = products;
		},
		SET_CURRENT_LIST(state, list) {
			state.currentList = list;
		},
		SET_STATUSES(state, statuses) {
			state.statuses = statuses;
		},
		SET_CART_DELIVERY_METHOD(state, method) {
			state.cart.delivery_method = method;
			computer.recompute(state.cart);
		},
		SET_CART_REGION_WITHOUT_RECALCULATION(state, region) {
			state.cart.region = region;
		},
		SET_CART_DELIVERY_METHOD_WITHOUT_RECALCULATION(state, method) {
			state.cart.delivery_method = method;
		},
		CHANGE_CART_ITEM(state, { item_index, new_product }) {
			let product = state.cart.items[item_index].product;
			product.base_price = new_product.base_price;
			product.price = new_product.price;
			product.discount = new_product.discount;
			computer.recompute(state.cart);
			window.store.set('cart', state.cart.items);
		},
		SET_BASE_URL(state, baseUrl) {
			state.baseUrl = baseUrl;
		}
	},
	actions: {
		// load_cart: async ({ commit, state }, vue) => {
			// let cart = (await vue.$api.post('get_cart')).data;
			// commit('SET_CART', cart.items);
		// },
		load_wishlist: ({ commit, state }) => {
			commit('SET_WISHLIST', window.store.get('wishlist') || []);
		},
		load_recently: ({ commit, state }) => {
			commit('SET_RECENTLY', window.store.get('recently') || []);
		},
		set_cart_region: ({ commit, state }, region) => {
			// debugger;
			commit('SET_CART_REGION_WITHOUT_RECALCULATION', region);
			let default_delivery = [[1,state.cart.region.delivery_courier,'courier'], [2,state.cart.region.delivery_pvz,'pvz'], [3,state.cart.region.delivery_post,'post']].filter(f => f[1]).sort()[0][2];
			commit('SET_CART_DELIVERY_METHOD_WITHOUT_RECALCULATION', default_delivery);
			computer.recompute(state.cart);
		},
		async nuxtServerInit({ commit, dispatch }, { req }) {
			if(req.user) commit('SET_USER', req.user);
			commit('SET_BASE_URL', req.baseUrl);
			commit('SET_CART', req.cart.items);
			commit('SET_CART_DELIVERY_METHOD', 'courier');
		}
	},
	getters: {
		catalog_list: state => state.lists.filter(f => f.in_catalog),
		menu_list: state => state.lists.filter(f => f.in_menu),
		root_list: state => state.lists.filter(f => f.parent_id == null && !f.in_main),
		root_list_wm: state => state.lists.filter(f => f.parent_id == null),
		reverse_cart_items: state => state.cart.items.slice().reverse(),
	}
});

export default store;