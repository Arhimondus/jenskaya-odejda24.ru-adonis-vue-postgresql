DECLARE
total_rate SMALLINT;
BEGIN
  SELECT ceil(AVG(rate)) INTO total_rate FROM jsonb_to_recordset(NEW.reviews) as x(rate int);
	IF(total_rate IS NOT null) THEN
		UPDATE products SET rate = total_rate WHERE product_id = NEW.product_id;
	END IF;
	RETURN NULL;
END