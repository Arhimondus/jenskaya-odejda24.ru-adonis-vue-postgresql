DECLARE
sizes_count SMALLINT;
BEGIN
	SELECT SUM((value::text::numeric)) INTO sizes_count FROM jsonb_each(NEW.sizes);
	UPDATE products SET count = sizes_count WHERE product_id = NEW.product_id;
	RETURN NULL;
END