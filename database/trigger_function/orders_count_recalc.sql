DECLARE
orders_count SMALLINT;
BEGIN
	--Routine body goes here...
	if(NEW.user_id IS NOT null) THEN
		SELECT COUNT(*) INTO orders_count FROM orders WHERE user_id = NEW.user_id AND status_id = 4;
		UPDATE users SET orders = orders_count WHERE user_id = NEW.user_id;
	END IF;
	RETURN NULL;
END